![
](https://bitbucket.org/repo/8d9gGb/images/211075167-logo_947x256_2.png)

**Galaxy Conflict** is a real time strategy game created by **Sea of Goblets** during the first **libGDX Game Jam**.

To win this game the player has to gain control over all planets of the galaxy by eliminating the 4 competiting factions. The planets, which differ in size and productivity, orbit around a central sun. At the beginning of the game each faction only controls its homeplanet. The other planets are inhabited by neutral and peace loving civilizations. 

**Controls:**
Using the left mouse button the player can select one or more of his planets. A drag and drop gesture between the selection and a target planet sends half of the ships residing on the planet(s). Additional tapping on the target planet sends additional ships. 

![attack3.png](https://bitbucket.org/repo/8d9gGb/images/3256958849-attack3.png)

If the target planet is owned by another faction the fighters attack the planet. The opposing forces cause equal losses on both sides. The planet is conquered when only attackers are alive.
The right mouse button or the cursor keys are used for moving the camera. The mouse wheel or the keys A/D for zooming.

Each planet has two **production modes**:

![button_fighter_normal.png](https://bitbucket.org/repo/8d9gGb/images/2851290130-button_fighter_normal.png) Builds fighters for attacking and conquering other planets. The size of the planet corresponds to its productivity (big planets produce ships 3 times faster than small planets).

![button_infrastructure_normal.png](https://bitbucket.org/repo/8d9gGb/images/288775539-button_infrastructure_normal.png) Improves the infrastructure of the planet. For each completed level the ship production is speed up by 100%. Each level takes twice as long to complete than the previous. The maximum level is 3, which boosts the planet's fighter production by 300%! 

## Credits: ##
### Programming: ###
Sea of Goblets

### Graphics: ###
Sea of Goblets

### Logo Font: ###
Galaxy 1 by Iconian Fonts http://www.iconian.com/

### Music: ###
Void Fleet from F4LL0UT http://f4ll0ut.newgrounds.com/

### Sound: ###
juskiddink http://freesound.org/people/juskiddink/

Nbs Dark http://freesound.org/people/Nbs%20Dark/

Infobandit http://freesound.org/people/infobandit/

kalisemorrison http://freesound.org/people/kalisemorrison/

danielnieto7 http://freesound.org/people/danielnieto7/

### Known issues: ###
Mouse picking does not work very well after resizing the window

**Feel free to contact us: seaofgoblets@gmail.com**