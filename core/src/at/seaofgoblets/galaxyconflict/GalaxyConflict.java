package at.seaofgoblets.galaxyconflict;

import at.seaofgoblets.galaxyconflict.view.screens.LoadingScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;

public class GalaxyConflict extends Game {
	private final AssetManager assetManager = new AssetManager();

	
	/*
	 *  - Pfeil beim draggen, bzw Linie
	 *  - wenn Planet als Ziel festgelegt wird, soll er blinken bzw. visuelles Feedback
	 *  + KI: wenn Prod fertig, 
	 *      - planet angriff mit KIndern oder mit Geschwistern, schick an Parent, oder er tut gar nix
	 *        - greift an Kind, Geswister oder Parent, n�hster planet
	 *      - mit 50, 75 0der100% der Figther 
	 *  - KI: 5 Fighter und dann Fabrik und dann wieder 5 Fighter ....
     *  - Alertsound wenn Feinde auftauchen, wenn scan und wenn agonizing Liste ist leer
	 *  - Visuelles Feedback wenn Produktion fertig
	 *  - ViewEvents & LogicEvents überdenken, ob man nicht Events weiterreichen kann
	 *  - Grafiken aufräumen 
	 *  - w�hrend angriff ziel hervorheben
	 *  
	 *  
	 *  
	 *  
	 *  
	 *  Bugs
	 *  -  ab und zu werden feindliche Fighter nicht ausgeblendet
	 *  - Spiel wird verzerrt wenn man die Fenstergröße ändert
	 *  - Partikeleffekte schöner machen
	 *  - Beim Kampf pro Planet nur mehr 1 Partikelemitter
	 *  - victory wird nich angezeigt
	 *  
	 *  Todo nach Jam
	 *  - KI verwendet Infrastruktur Upgrade
	 *  - Anzeige Infrastruktur Level
	 *  - Neutrale Planeten erholen sich
	 *  - �berlegenheit bei Kampf einbeziehen, Wahrscheinlichkeiten beim Kmpf, je mehr Verteidiger desto h�her die Wahrscheinlichkeit dass Angreifer verliert
	 *  - wenn man die maximale infrastruktur stufe erreicht hat, automatisch auf fighter umschalten
	 *   - musik abdrehen mit M
	 *   
	 *   
	 *   
	 *   Grafiken:
	 *   
	 *   - Progressbar Planet
	 *   - Römische Zahlen 1-3
	 *   - Schiffe + Antriebi
	 * */
	
	
	@Override
	public void create() {
		setScreen(new LoadingScreen(this));
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}
}
