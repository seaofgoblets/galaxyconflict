package at.seaofgoblets.galaxyconflict.logic.observer;

public interface Observer {
	void update(SubjectMessage message);
}
