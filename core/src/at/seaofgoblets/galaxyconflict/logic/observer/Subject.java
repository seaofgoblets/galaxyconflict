package at.seaofgoblets.galaxyconflict.logic.observer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

public class Subject {

	private static final String TAG = Subject.class.getName();

	private Array<Observer> observers = new Array<Observer>();

	public void addObserver(Observer observer) {
		observers.add(observer);

		Gdx.app.log(TAG, "Added observer, count " + observers.size);
	}

	public void removeObserver(Observer observer) {
		observers.removeValue(observer, true);

		Gdx.app.log(TAG, "Removed observer, count " + observers.size);
	}

	public void notifyAllObservers(SubjectMessage message) {
		for (Observer observer : observers) {
			observer.update(message);
		}
	}
}
