package at.seaofgoblets.galaxyconflict.logic.observer;

public class ProductionCompletedMessage implements SubjectMessage{
	
	private final int numStepsNextProduction;
	
	public ProductionCompletedMessage(int numStepsNextProduction){
		this.numStepsNextProduction = numStepsNextProduction;
	}	
	
	public int getNumStepsNextProduction(){
		return this.numStepsNextProduction;
	}

}
