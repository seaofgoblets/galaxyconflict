package at.seaofgoblets.galaxyconflict.logic.observer;

public class ProductionModeChangedMessage implements SubjectMessage{
	private final int step;
	private final int totalSteps;
	
	public ProductionModeChangedMessage(int step, int totalSteps) {
		super();
		this.step = step;
		this.totalSteps = totalSteps;
	}

	public int getStep() {
		return step;
	}

	public int getTotalSteps() {
		return totalSteps;
	}		
}
