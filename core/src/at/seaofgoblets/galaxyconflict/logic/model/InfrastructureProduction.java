package at.seaofgoblets.galaxyconflict.logic.model;

import at.seaofgoblets.galaxyconflict.logic.model.Planet.ProductionMode;
import at.seaofgoblets.galaxyconflict.logic.observer.ProductionCompletedMessage;
import at.seaofgoblets.galaxyconflict.logic.observer.ProductionStepCompletedMessage;

public class InfrastructureProduction implements Production{

	public static final String TAG = InfrastructureProduction.class.getName();
	
	private final Planet planet;

	private static final int MAX_INFRASTRUCTURE_LEVEL = 3;
	private static final int[] STEPS = new int[]{ 40,80,160};
	
	private int targetValue = STEPS[0];
	private float currentValue;
	
	private int lastValue;
	
	private final ProductionMode mode = ProductionMode.INFRASTRUCTURE;
			
	public InfrastructureProduction(Planet planet){
		this.planet = planet;
	}
			
	@Override
	public void update(float deltaTime){
		currentValue += planet.getCategory().getProductivity() * deltaTime;
		
		int value = (int)currentValue;
		
		if(value != lastValue){
			lastValue = value;
			
			planet.notifyAllObservers(new ProductionStepCompletedMessage(value, targetValue));
		}		
		
		if(value >= targetValue){
									
			currentValue = 0;
			
			int level = planet.getInfrastructureLevel();
			
			int newLevel = level < MAX_INFRASTRUCTURE_LEVEL ? level +1 : level;
			
			planet.reachedNextInfrastructureLevel(newLevel);
			
			targetValue = STEPS[newLevel-1];
			
			planet.notifyAllObservers(new ProductionCompletedMessage(targetValue));
		}
	}

	
	@Override
	public ProductionMode getMode() {
		return mode;
	}
	
	@Override
	public int getCurrentStep() {		
		return lastValue;
	}

	@Override
	public int getNumTotalSteps() {
		return targetValue;
	}
	
	@Override
	public float getPercentage(){
		return currentValue / targetValue;		
	}
}
