package at.seaofgoblets.galaxyconflict.logic.model;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import at.seaofgoblets.galaxyconflict.logic.controller.GameState;
import at.seaofgoblets.galaxyconflict.logic.event.FighterArrivedEvent;
import at.seaofgoblets.galaxyconflict.logic.event.FighterDestroyedEvent;
import at.seaofgoblets.galaxyconflict.logic.event.ScanPlanetEvent;

public class Fighter extends Orbiter {

	private static final float ORBITING_DISTANCE_FACTOR_DEFENSE = 0.7f;
	private static final float ORBITING_DISTANCE_FACTOR_ATTACK = 0.9f;
	private static final float MIN_DISTANCE = 10;
	private static final float VELOCITY = 20;
	private static final float ANGULAR_VELOCITY = 0.25f;
	private static final float SCAN_DISTANCE = 50;
	private static final float AGONY_TIME = 3;

	private final GameState gameState;

	private Planet homePlanet;

	private Vector2 direction = new Vector2();

	private float rotation;

	private float agonyTime = 0;
	private boolean agony = false;

	public static final int DEFENSE = 1;
	public static final int ATTACK = -1;
	private int mode = DEFENSE;
	
	private float random = MathUtils.random();

	private boolean transfering = false;

	public Fighter(Faction faction, GameState gameState) {
		this.faction = faction;
		this.gameState = gameState;
	}

	public void createAt(Planet homePlanet) {

		x = homePlanet.getX();
		y = homePlanet.getY();

		attachTo(homePlanet);
		this.homePlanet.getOrbitingFighters().add(this);
	}

	public void attachTo(Planet homePlanet) {
		this.homePlanet = homePlanet;
		updateOrbitingDistance(homePlanet);
	}

	public void sendTo(Planet targetPlanet) {		
		this.homePlanet = targetPlanet;

		updateOrbitingDistance(targetPlanet);
		transfering = true;
	}

	public void update(float delta) {

		if (agony) {
			orbitingDistance *= 1 - (0.0025f * (2 + MathUtils.cos(orbitingAngle * 3)));
		}

		orbitingAngle = orbitingAngle + (random * 0.1f + ANGULAR_VELOCITY) * delta * mode;

		float targetX = homePlanet.getX();
		float targetY = homePlanet.getY();
		targetX += (float) MathUtils.cos(orbitingAngle) * orbitingDistance;
		targetY += (float) MathUtils.sin(orbitingAngle) * orbitingDistance;

		Vector2 range = new Vector2(targetX - x, targetY - y);
		float speed = VELOCITY * delta + (1 + MathUtils.cos(random + orbitingAngle * 3)) / 8;

		if (range.len() > speed)
			range.setLength(speed);

		x += range.x;
		y += range.y;

		if (!agony) {
			float newOrbitingAngle = orbitingAngle + ANGULAR_VELOCITY * delta * 5 * mode;
			targetX = homePlanet.getX() + (float) MathUtils.cos(newOrbitingAngle) * orbitingDistance;
			targetY = homePlanet.getY() + (float) MathUtils.sin(newOrbitingAngle) * orbitingDistance;
		} else {
			targetX = homePlanet.getX();
			targetY = homePlanet.getY();
		}
		range = new Vector2(targetX - x, targetY - y);
		rotation = MathUtils.lerpAngleDeg(rotation, range.angle(), 0.075f);

		if (transfering) {
			float distance = new Vector2(targetX - x, targetY - y).len();

			if (distance < SCAN_DISTANCE) {
				gameState.addEvent(new ScanPlanetEvent(this, homePlanet));
			}

			if (distance < orbitingDistance) {
				gameState.addEvent(new FighterArrivedEvent(this, homePlanet));
				transfering = false;
			}
		}

		if (agony) {
			agonyTime += delta;
			if (agonyTime >= AGONY_TIME) {
				gameState.addEvent(new FighterDestroyedEvent(this));
			}
		}
	}

	public Planet getHomePlanet() {
		return this.homePlanet;
	}

	public float getOrbitingDistance() {
		return this.orbitingDistance;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	private void updateOrbitingDistance(Planet planet) {
		float orbitingFactor = mode == DEFENSE ? ORBITING_DISTANCE_FACTOR_DEFENSE : ORBITING_DISTANCE_FACTOR_ATTACK;
		this.orbitingDistance = planet.getCategory().getSize() * orbitingFactor;
		if (orbitingDistance < MIN_DISTANCE)
			orbitingDistance = MIN_DISTANCE;
	}

	public Vector2 getDirection() {
		return this.direction;
	}

	public void startAgony() {
		agony = true;
	}

	public float getRotation() {
		return rotation;
	}
}
