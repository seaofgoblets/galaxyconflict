package at.seaofgoblets.galaxyconflict.logic.model;

import at.seaofgoblets.galaxyconflict.logic.model.Planet.ProductionMode;

public interface Production {

	void update(float deltaTime);
	
	ProductionMode getMode();
	
	int getCurrentStep();
	
	int getNumTotalSteps();
	
	float getPercentage();
}
