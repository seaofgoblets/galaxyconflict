package at.seaofgoblets.galaxyconflict.logic.model;

public class LogicConfig {

	public static final PlanetCategory PLANET_SUN = new PlanetCategory("SUN", 45, 0.3f,0);	
	public static final PlanetCategory PLANET_BIG = new PlanetCategory("BIG", 30, 3,5);
	public static final PlanetCategory PLANET_MEDIUM = new PlanetCategory("MEDIUM", 20, 2,3);
	public static final PlanetCategory PLANET_SMALL = new PlanetCategory("SMALL", 10, 1,2);
	
	public static final int PRODUCTION_FIGHTER = 0;
	public static final int PRODUCTION_INFRASTRUCTURE = 1;
	public static final int DEFAULT_PRODUCTION = PRODUCTION_FIGHTER;
}
