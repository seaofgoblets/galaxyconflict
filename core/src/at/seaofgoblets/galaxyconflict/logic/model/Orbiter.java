package at.seaofgoblets.galaxyconflict.logic.model;

public class Orbiter {

	protected float x;
	protected float y;
		
	protected float orbitingAngle;
	protected float orbitingDistance;
	
	protected Faction faction;
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}

	public void setX(float x) {
		this.x = x;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public Faction getFaction(){
		return faction;
	}
	
	public void setFaction(Faction faction){
		this.faction = faction;
	}
	
	public void setOrbitingAngle(float angle){
		this.orbitingAngle = angle;
	}
}
