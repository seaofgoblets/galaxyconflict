package at.seaofgoblets.galaxyconflict.logic.model;

public class PlanetCategory {
	private final String name;
	
	private final float productivity;
	
	private final int size;
	
	private final int numFighters;
		
	public PlanetCategory(String name, int size,
			float productivity, int numFighters) {
		super();
		this.name = name;
		this.size = size;
		this.productivity = productivity;
		this.numFighters = numFighters;
	}
	public String getName() {
		return name;
	}
	
	public float getProductivity() {
		return productivity;
	}
	
	public int getSize(){
		return size;
	}		
	
	public int getNumFighters(){
		return numFighters;
	}
}
