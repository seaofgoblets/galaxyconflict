package at.seaofgoblets.galaxyconflict.logic.model;

import at.seaofgoblets.galaxyconflict.logic.event.CreateFighterEvent;
import at.seaofgoblets.galaxyconflict.logic.model.Planet.ProductionMode;
import at.seaofgoblets.galaxyconflict.logic.observer.ProductionCompletedMessage;
import at.seaofgoblets.galaxyconflict.logic.observer.ProductionStepCompletedMessage;

public class FighterProduction implements Production{
	
	private final Planet planet;
	
	private int targetValue = 20;
	private float currentValue;
	
	private int lastValue;
	
	private final ProductionMode mode = ProductionMode.FIGHTER;
			
	public FighterProduction(Planet planet){
		this.planet = planet;
	}
			
	@Override
	public void update(float deltaTime){
		currentValue += ((planet.getInfrastructureLevel() + planet.getCategory().getProductivity()) * deltaTime);
		
		int value = (int)currentValue;
		
		if(value != lastValue){
			lastValue = value;			
			planet.notifyAllObservers(new ProductionStepCompletedMessage(value, targetValue));
		}		
		
		if(value >= targetValue){
			
			this.planet.getGameState().addEvent(new CreateFighterEvent(this.planet)); 			
			currentValue = 0;			
			planet.notifyAllObservers(new ProductionCompletedMessage(targetValue));
		}
	}

	@Override
	public ProductionMode getMode() {
		return mode;
	}

	@Override
	public int getCurrentStep() {		
		return lastValue;
	}

	@Override
	public int getNumTotalSteps() {
		return targetValue;
	}
	
	@Override
	public float getPercentage(){
		return currentValue / targetValue;		
	}
}
