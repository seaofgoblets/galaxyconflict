package at.seaofgoblets.galaxyconflict.logic.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

import at.seaofgoblets.galaxyconflict.logic.controller.GameState;
import at.seaofgoblets.galaxyconflict.logic.observer.Observer;
import at.seaofgoblets.galaxyconflict.logic.observer.ProductionModeChangedMessage;
import at.seaofgoblets.galaxyconflict.logic.observer.Subject;
import at.seaofgoblets.galaxyconflict.logic.observer.SubjectMessage;

public class Planet extends Orbiter {

	private static final String TAG = Planet.class.getName();

	public enum ProductionMode {
		INFRASTRUCTURE, FIGHTER
	}

	private String name;

	private Planet parent;
	private float velocity;

	private final GameState gameState;
	private final PlanetCategory category;

	private Array<Planet> orbitingPlanets = new Array<Planet>();
	private Array<Fighter> orbitingFighters = new Array<Fighter>();

	private Array<Fighter> agonizingFighters = new Array<Fighter>();

	private InfrastructureProduction infrastructureProduction = new InfrastructureProduction(
			this);
	private Production fighterProduction = new FighterProduction(this);

	private Production currentProduction = fighterProduction;

	private Subject subject = new Subject();

	private int infrastructureLevel = 1;

	public Planet(String name, PlanetCategory category, GameState gameState) {
		this.name = name;
		this.category = category;
		this.gameState = gameState;
	}

	public void update(float delta) {

		if (parent != null) {
			orbitingAngle = orbitingAngle + velocity * delta;

			x = parent.x + (float) MathUtils.cos(orbitingAngle)
					* orbitingDistance;
			y = parent.y + (float) MathUtils.sin(orbitingAngle)
					* orbitingDistance;
		}

		if (orbitingPlanets != null) {
			for (Planet planet : orbitingPlanets) {
				planet.update(delta);
			}
		}
	}

	public void updateProduction(float delta) {
		currentProduction.update(delta);
	}

	public void attachTo(Planet parent, float orbitingDistance, float angle,
			float velocity) {
		this.parent = parent;
		this.parent.getOrbitingPlanets().add(this);
		this.orbitingDistance = orbitingDistance;
		this.orbitingAngle = angle;
		this.velocity = velocity;
	}

	public void removeOrbitingFighter(Fighter fighter) {
		this.orbitingFighters.removeValue(fighter, true);

		Gdx.app.log(TAG, "Removed 1 orbiting fighter, "
				+ this.orbitingFighters.size + " fighters left");
	}

	public void removeAgonizingFighter(Fighter fighter) {
		this.agonizingFighters.removeValue(fighter, true);

		Gdx.app.log(TAG, "Removed 1 agonizing fighter, "
				+ this.agonizingFighters.size + " fighters left");
	}

	public void combat(Fighter incomingFighter) {
		//
		// infrastructureProduction.applyDamage();

		incomingFighter.attachTo(this);
		agonizingFighters.add(incomingFighter);
		incomingFighter.startAgony();

		Fighter defendingFighter = orbitingFighters
				.get(orbitingFighters.size - 1);
		defendingFighter.startAgony();
		orbitingFighters.removeValue(defendingFighter, true);
		agonizingFighters.add(defendingFighter);
	}

	public PlanetCategory getCategory() {
		return this.category;
	}

	public Planet getParent() {
		return this.parent;
	}

	public Array<Planet> getOrbitingPlanets() {
		return orbitingPlanets;
	}

	public void addOrbitingPlanets(Array<Planet> planets) {
		orbitingPlanets.addAll(planets);
	}

	public Array<Fighter> getOrbitingFighters() {
		return orbitingFighters;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProductionMode getProductionMode() {
		return this.currentProduction.getMode();
	}

	public void setProductionMode(ProductionMode mode) {
		switch (mode) {
		case INFRASTRUCTURE:
			currentProduction = infrastructureProduction;
			break;
		case FIGHTER:
		default:
			currentProduction = fighterProduction;
			break;
		}

		notifyAllObservers(new ProductionModeChangedMessage(
				currentProduction.getCurrentStep(),
				currentProduction.getNumTotalSteps()));
	}

	public void addObserver(Observer observer) {
		this.subject.addObserver(observer);
	}

	public void removeObserver(Observer observer) {
		this.subject.removeObserver(observer);
	}

	public void notifyAllObservers(SubjectMessage message) {
		this.subject.notifyAllObservers(message);
	}

	public GameState getGameState() {
		return this.gameState;
	}

	public int getInfrastructureLevel() {
		return this.infrastructureLevel;
	}

	public void setInfrastructureLevel(int level) {
		this.infrastructureLevel = level;
	}

	public void reachedNextInfrastructureLevel(int level) {
		this.infrastructureLevel = level;
	}

	public Array<Fighter> getFighters(int numFighters) {
		Array<Fighter> fighters = new Array<Fighter>();

		for (int f = 0; f < numFighters; f++) {
			fighters.add(orbitingFighters.get(f));
		}

		return fighters;
	}

	public Array<Fighter> getFighters(float percentage) {
		return getFighters((int) (orbitingFighters.size * percentage));
	}

	public int getNumAgonizingFighters() {
		return agonizingFighters.size;
	}

	public float getProductionPercentage() {
		return currentProduction.getPercentage();
	}
}
