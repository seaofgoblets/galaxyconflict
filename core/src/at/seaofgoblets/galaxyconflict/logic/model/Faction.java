package at.seaofgoblets.galaxyconflict.logic.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;

public class Faction {
	private final String name;
	private final String homePlanetName;
	private final Color color;	
	private final Array<Planet> planets = new Array<Planet>();
	private int numFighters;

	public Faction(String name, String homePlanetName, Color color){
		this.name = name;		
		this.color = color;
		this.homePlanetName = homePlanetName;
	}
	
	public String getName() {
		return name;
	}
	
	public String getHomePlanetName() {
		return homePlanetName;
	}
	
	public Color getColor(){
		return color;
	}	
			
	public Array<Planet> getPlanets() {
		return planets;
	}

	public void update(float deltaTime){
		for(Planet p : planets){
			p.updateProduction(deltaTime);
		}
	}		
	
	public int getNumFighters(){
		return this.numFighters;
	}
	
	public void addFighter(){
		this.numFighters++;
	}
	
	public void removeFighter(){
		this.numFighters--;
	}
}
