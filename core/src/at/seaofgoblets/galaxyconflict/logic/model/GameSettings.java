package at.seaofgoblets.galaxyconflict.logic.model;

public class GameSettings {

	private final int numPlanets;
	private final int numAIPlayers;
	
	public GameSettings(int numPlanets, int numAIPlayers) {
		super();
		this.numPlanets = numPlanets;
		this.numAIPlayers = numAIPlayers;
	}

	public int getNumPlanets() {
		return numPlanets;
	}

	public int getNumAIPlayers() {
		return numAIPlayers;
	}		
}
