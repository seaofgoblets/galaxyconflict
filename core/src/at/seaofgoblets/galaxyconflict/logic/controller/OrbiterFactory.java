package at.seaofgoblets.galaxyconflict.logic.controller;

import com.badlogic.gdx.utils.Array;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;
import at.seaofgoblets.galaxyconflict.logic.model.PlanetCategory;

public class OrbiterFactory{

	private static final Array<String> planetNames = new Array<String>(new String[] {
			"SOTHIUTER", "UBREURIA", "XOTHADUS", "ASMUNA", "CUONUS", "BUOSTEA",
			"STUFORILIA", "SPECUTUNE", "THESHAN", "GRONE", "GOTRIEHIRI",
			"YAPLEYYAMA", "DEPROSIE", "QESHIDES", "KOENOPE", "DEPHUS",
			"TRECATER", "SLODOTIS", "TRIDES", "SMOSIE", "FUGLOYCURY",
			"QATRARILIA", "XAGRARTH", "UDRICHI", "DAUCLITE", "IEBOS",
			"BLUFETANIA", "CRABANOV", "SWORA", "CHICHI", "USLOUTERA",
			"ZATREINUS", "QUFROMIA", "LOWHORE", "OALARA", "OIZUNO", "SKABAVIS",
			"STRUFOSTEA", "PLIRI", "CLERTH", "BECHOHIRI", "YEWHEORIA",
			"OCROLLA", "IOCLARTH", "UENERTH", "CUONOV", "STRODORUS",
			"BLABOGANTU", "STRAGUA", "CRIUQ", "CESKOETUNE", "LATRIUTOV",
			"VESHOV", "OSWORIA", "FUIPRA", "OURILIA", "STRABAVIS", "FLODANUS",
			"SLILLON", "STRIRI", "TUSCAERIA", "RESPAUTOV", "IUSNONE",
			"ASTRYKE", "PIEBOS", "BOATER", "STABECARRO", "PLECULIV", "THYRIA",
			"GRION", "JUAMS", "VU", "ZLIUC", "TROATH", "KU", "BROOCCOT",
			"YUNNAA", "RAEGTASZ", "SHEONNIM", "UTRUACLUSZ", "XA", "VLOKS",
			"KLIOCZ", "THU", "TROCZ", "ACRUSLESH", "EROOTHESH", "OFAICHESZ",
			"QAITLUH", "EBRUGSACT", "IZOIKS", "QED", "MEHS", "PAALT", "CHEUTH",
			"JEHURS", "ITREJEEHS", "THEATOLS", "ZRAEMLOTS", "HEUDRACS",
			"RIALT", "VLAPH", "UKROSZ", "QUY", "ZATS", "GIUSHUB", "CHAESDUTS",
			"VLAIMDUMS", "ZEOZTA", "FUKI" });
	
	private static final OrbiterFactory instance = new OrbiterFactory();

	private int planetNameCounter;	
	
	private GameState gameState;
	
	public static OrbiterFactory getInstance(){
		return instance;
	}
	
	public void setGameState(GameState gameState){
		this.gameState = gameState;
	}
		
	public Planet buildPlanet(PlanetCategory category){		
		return new Planet(planetNames.get(planetNameCounter++), category, gameState);
	}
	
	public Planet buildPlanet(String planetName, PlanetCategory category){		
		return new Planet(planetName, category, gameState);
	}
	
	public Fighter buildFighter(Planet planet){		
		return new Fighter(planet.getFaction(), gameState);		
	}
}
