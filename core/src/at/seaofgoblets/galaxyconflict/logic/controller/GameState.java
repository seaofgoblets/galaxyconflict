package at.seaofgoblets.galaxyconflict.logic.controller;

import at.seaofgoblets.galaxyconflict.logic.ai.controller.AIController;
import at.seaofgoblets.galaxyconflict.logic.ai.event.AICommand;
import at.seaofgoblets.galaxyconflict.logic.ai.event.AISendFighterCommand;
import at.seaofgoblets.galaxyconflict.logic.event.CreateFighterEvent;
import at.seaofgoblets.galaxyconflict.logic.event.FighterArrivedEvent;
import at.seaofgoblets.galaxyconflict.logic.event.FighterDestroyedEvent;
import at.seaofgoblets.galaxyconflict.logic.event.LogicEvent;
import at.seaofgoblets.galaxyconflict.logic.event.ScanPlanetEvent;
import at.seaofgoblets.galaxyconflict.logic.event.SendFightersEvent;
import at.seaofgoblets.galaxyconflict.logic.model.Faction;
import at.seaofgoblets.galaxyconflict.logic.model.Fighter;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;
import at.seaofgoblets.galaxyconflict.view.event.CreateFighterSpriteEvent;
import at.seaofgoblets.galaxyconflict.view.event.DestroyFighterSpriteEvent;
import at.seaofgoblets.galaxyconflict.view.event.FactionGameOverEvent;
import at.seaofgoblets.galaxyconflict.view.event.PlanetBattleEvent;
import at.seaofgoblets.galaxyconflict.view.event.PlanetStatusChangedEvent;
import at.seaofgoblets.galaxyconflict.view.event.ShowIncomingFighterEvent;
import at.seaofgoblets.galaxyconflict.view.event.ShowPlanetFighterEvent;
import at.seaofgoblets.galaxyconflict.view.event.ViewEventHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Queue;

public class GameState {

	private static final String TAG = GameState.class.getName();

	private Array<Planet> planets;
	private Array<Fighter> fighters;

	private Array<Faction> factions;
	private Faction playerFaction;
	private int numActiveFactions;

	private final Queue<LogicEvent> events = new Queue<LogicEvent>();
	private LogicEvent currentEvent = null;

	private ViewEventHandler viewEventHandler;

	private final float[] timeLapseSteps = new float[] { 0.5f, 1, 2, 5, 10, 25, 50, 100 };
	private int currentTimeLapseIndex = 1;
	private float timeLapseMultiplier = timeLapseSteps[currentTimeLapseIndex];

	private boolean showFightersAllFactions = false;
	
	private AIController aiController;

	public void setAiController(AIController aiController) {
		this.aiController = aiController;
	}

	public void setViewEventHandler(ViewEventHandler viewEventHandler) {
		this.viewEventHandler = viewEventHandler;
	}

	public void setPlanets(Array<Planet> planets) {
		this.planets = planets;
	}

	public Array<Planet> getPlanets() {
		return planets;
	}

	public Array<Faction> getFactions() {
		return factions;
	}

	public void addFactions(Array<Faction> factions) {
		this.factions = factions;
		numActiveFactions+=factions.size;
	}

	public Faction getPlayerFaction() {
		return playerFaction;
	}

	public void addPlayerFaction(Faction playerFaction) {
		this.playerFaction = playerFaction;
		numActiveFactions++;
	}

	public Array<Fighter> getFighters() {
		return fighters;
	}

	public void setFighters(Array<Fighter> fighters) {
		this.fighters = fighters;
	}

	public void update(float deltaTime) {

		planets.get(0).update(deltaTime);

		playerFaction.update(deltaTime);
		for (Faction f : factions) {
			f.update(deltaTime);
		}

		for (Fighter f : fighters) {
			f.update(deltaTime);
		}

		handleEvents();
	}

	public void addEvent(LogicEvent event) {
		events.addLast(event);
	}

	public void handleEvents() {

		while (events.size > 0) {
			if (currentEvent == null) {
				currentEvent = events.removeFirst();
			}

			if (currentEvent instanceof CreateFighterEvent) {
				CreateFighterEvent event = (CreateFighterEvent) currentEvent;
				Planet planet = event.getPlanet();
				createFighter(planet);

				if (planet.getFaction() != null && planet.getFaction() != playerFaction) {
					Array<AICommand> aiCommands = aiController.planNextAction(planet);
					
					for(AICommand aiCommand : aiCommands){
						if(aiCommand instanceof AISendFighterCommand){
							AISendFighterCommand e = (AISendFighterCommand) aiCommand;
							sendFighters(e.getFighters(), e.getTargetPlanet());
						}
					}
				}
			} else if (currentEvent instanceof SendFightersEvent) {
				SendFightersEvent event = (SendFightersEvent) currentEvent;				
				sendFighters(event.getFighters(), event.getTargetPlanet());
			} else if (currentEvent instanceof ScanPlanetEvent) {
				ScanPlanetEvent scanPlanetEvent = ((ScanPlanetEvent) currentEvent);
				Planet planet = scanPlanetEvent.getPlanet();
				Faction scannningFaction = scanPlanetEvent.getFighter().getFaction();
				Fighter fighter = scanPlanetEvent.getFighter();
				if (scannningFaction == playerFaction) {
					viewEventHandler.addEvent(new ShowPlanetFighterEvent(fighter, planet));
				}else if (planet.getFaction() == playerFaction) {
					viewEventHandler.addEvent(new ShowIncomingFighterEvent(fighter));
				}
			} else if (currentEvent instanceof FighterArrivedEvent) {
				FighterArrivedEvent fighterArrivedEvent = (FighterArrivedEvent) currentEvent;

				Fighter fighter = fighterArrivedEvent.getFighter();
				Planet planet = fighterArrivedEvent.getPlanet();

				if (planet.getFaction() == fighter.getFaction()) {
					fighter.setMode(Fighter.DEFENSE);
					fighter.attachTo(planet);

					planet.getOrbitingFighters().add(fighter);
					Gdx.app.log(TAG, "Fighter arrived at " + planet.getName() + ", making "
							+ planet.getOrbitingFighters().size + " fighters");
				} else {
					if (planet.getOrbitingFighters().size > 0) {
						fighter.setMode(Fighter.ATTACK);
						Gdx.app.log(TAG, "Fighter attacks " + planet.getName() + " and fights against "
								+ planet.getOrbitingFighters().size + " fighters.");
						planet.combat(fighter);
						
						viewEventHandler.addEvent(new PlanetBattleEvent(fighter, planet));
					} else {
						Gdx.app.log(TAG, planet.getName() + " conquered by  " + fighter.getFaction().getName());
						fighter.setMode(Fighter.DEFENSE);
						if (planet.getFaction() != null) {
							Faction faction =planet.getFaction(); 
							faction.getPlanets().removeValue(planet, true);
							
							if(faction.getPlanets().size == 0 && faction.getNumFighters() == 0){
								viewEventHandler.addEvent(new FactionGameOverEvent(faction));
								numActiveFactions--;
							}
						}												 
						
						planet.setFaction(fighter.getFaction());

						fighter.getFaction().getPlanets().add(planet);
						fighter.attachTo(planet);

						planet.getOrbitingFighters().add(fighter);

						viewEventHandler.addEvent(new PlanetStatusChangedEvent(planet,PlanetStatusChangedEvent.FACTION_CHANGED));
						//planet.notifyAllObservers(new PlanetChangedMessage());
					}
				}
			} else if (currentEvent instanceof FighterDestroyedEvent) {
				FighterDestroyedEvent fighterDestroyedEvent = (FighterDestroyedEvent) currentEvent;
				Fighter fighter = fighterDestroyedEvent.getFighter(); 
				Faction faction = fighter.getFaction();
				destroyFighter(fighter);
				
				if(faction != null && faction.getPlanets().size == 0 && faction.getNumFighters() == 0){
					viewEventHandler.addEvent(new FactionGameOverEvent(faction));
					numActiveFactions--;
				}
			}

			currentEvent = null;
		}
	}

	private void createFighter(Planet planet) {
		Fighter fighter = OrbiterFactory.getInstance().buildFighter(planet);
		fighter.createAt(planet);

		fighters.add(fighter);
		fighter.getFaction().addFighter();
		if (planet.getFaction() == playerFaction || showFightersAllFactions) {
			viewEventHandler.addEvent(new CreateFighterSpriteEvent(fighter));
		}
	}

	private void destroyFighter(Fighter fighter) {
		if (fighter.getHomePlanet() != null) {
			fighter.getHomePlanet().removeAgonizingFighter(fighter);
		}
		
		if(fighter.getFaction() != null){
			fighter.getFaction().removeFighter();
		}
		
		fighters.removeValue(fighter, true);

		viewEventHandler.addEvent(new DestroyFighterSpriteEvent(fighter));
	}

	private static void sendFighters(Array<Fighter> fighters, Planet targetPlanet) {

		for (Fighter f : fighters) {
			f.getHomePlanet().removeOrbitingFighter(f);
			//f.getHomePlanet().notifyAllObservers(new PlanetChangedMessage());
		}
		
		for (Fighter f : fighters) {
			f.sendTo(targetPlanet);
		}
	}

	public void incrementeTimeLapse() {
		if (currentTimeLapseIndex < timeLapseSteps.length - 1) {
			currentTimeLapseIndex++;
			timeLapseMultiplier = timeLapseSteps[currentTimeLapseIndex];
		}
	}

	public void decrementeTimeLapse() {
		if (currentTimeLapseIndex > 0) {
			currentTimeLapseIndex--;
			timeLapseMultiplier = timeLapseSteps[currentTimeLapseIndex];
		}
	}

	public float getTimeLapseMultiplier() {
		return timeLapseMultiplier;
	}
	
	public int getNumActiveFactions(){
		return this.numActiveFactions;
	}
}
