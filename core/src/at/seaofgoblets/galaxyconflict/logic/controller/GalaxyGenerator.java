package at.seaofgoblets.galaxyconflict.logic.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

import at.seaofgoblets.galaxyconflict.logic.ai.controller.AIController;
import at.seaofgoblets.galaxyconflict.logic.model.Faction;
import at.seaofgoblets.galaxyconflict.logic.model.Fighter;
import at.seaofgoblets.galaxyconflict.logic.model.GameSettings;
import at.seaofgoblets.galaxyconflict.logic.model.LogicConfig;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;
import at.seaofgoblets.galaxyconflict.logic.model.PlanetCategory;
import at.seaofgoblets.galaxyconflict.view.model.ViewConfig;

public class GalaxyGenerator {
	
	private static final String TAG = GalaxyGenerator.class.getName();
//http://prideout.net/archive/colors.php
	private static final Faction[] allFactions = new Faction[]{
		new Faction("HUTIQUANS","HUTIQUA", new Color(0.698f, 0, 1,1)),
		new Faction("CH'KORERANS", "CH'KORERA", new Color(1, 0.847f, 0,1)),
		new Faction("ELASSIS", "ELASSI", new Color(0.714f, 1, 0,1)),
		new Faction("YEGRUANIANS", "YEGRUANIA", new Color(0,1,1,1)),
		new Faction("BLUYDANS","BLUYDA", new Color(0, 0.58f, 1,1)),
		new Faction("SHEZ DRANAOIDS","SHEZ DRANA", new Color(1, 0, 0.863f,1)),		
	};

	private static PlanetCategory[] planetCategoriesHuge = new PlanetCategory[]{
		LogicConfig.PLANET_BIG
	};
	
	private static PlanetCategory[] planetCategoriesMedium = new PlanetCategory[]{

		LogicConfig.PLANET_MEDIUM,
	};
	
	private static PlanetCategory[] planetCategoriesSmall = new PlanetCategory[]{
		LogicConfig.PLANET_SMALL
	};	
	
	public void generate(GameState gameState, GameSettings settings) {

		Array<Planet> planets = generatePlanets(settings.getNumPlanets());
		Array<Faction> factions = new Array<Faction>();
				
		Array<Planet> potentialStartPlanets = new Array<Planet>();
		for(Planet p : planets){
			if(p.getCategory() == LogicConfig.PLANET_BIG){
				potentialStartPlanets.add(p);
			}
		}
		potentialStartPlanets.shuffle();
		
		Faction playerFaction = new Faction("PLAYER","HOPE", new Color(1,0,0,1));
		Planet playerStartPlanet =potentialStartPlanets.get(0); 
		playerStartPlanet.setFaction(playerFaction);
		playerFaction.getPlanets().add(playerStartPlanet);
		
		for(int a = 0; a < settings.getNumAIPlayers(); a++){
			Faction faction = allFactions[a];
			Planet planet = potentialStartPlanets.get(a+1);
			
			factions.add(faction);
			planet.setFaction(faction);
			faction.getPlanets().add(planet);
		}		
		
		Array<Fighter> fighters = new Array<Fighter>();
		
		int numFighters;
		OrbiterFactory factory = OrbiterFactory.getInstance();
		for(Planet p : planets){
			if(p.getCategory() != LogicConfig.PLANET_SUN){
				numFighters = p.getCategory().getNumFighters();
				float angle = MathUtils.PI2 / numFighters;
				for(int f = 0; f < numFighters; f++){
					Fighter fighter = factory.buildFighter(p);
					fighter.attachTo(p);
					p.getOrbitingFighters().add(fighter);
					
					float orbitingAngle = angle * f;
					fighter.setOrbitingAngle(orbitingAngle);
			
					float x=p.getX() + (float)MathUtils.cos(orbitingAngle) * fighter.getOrbitingDistance();
					float y=p.getY() + (float)MathUtils.sin(orbitingAngle) * fighter.getOrbitingDistance();
					fighter.setX(x);
					fighter.setY(y);
					fighters.add(fighter);
					
					if(fighter.getFaction() != null){
						fighter.getFaction().addFighter();
					}
				}							
			}
		}
		
		updatePlanetNames(planets.get(0));

		gameState.setPlanets(planets);
		gameState.addFactions(factions);
		gameState.addPlayerFaction(playerFaction);
		gameState.setFighters(fighters);
		gameState.setAiController(new AIController(planets));		
		
		Gdx.app.log(TAG, "Generated " + planets.size + " planets.");
	}

	private Array<Planet> generatePlanets(int numPlanets) {
		Array<Planet> planets = new Array<Planet>();

		Planet sun = OrbiterFactory.getInstance().buildPlanet(LogicConfig.PLANET_SUN);
		sun.setX(ViewConfig.GALAXY_WIDTH / 2);
		sun.setY(ViewConfig.GALAXY_HEIGHT / 2);

		//MathUtils.random.setSeed(500);

		int depth = 3;
		float angleSlot = MathUtils.PI2 / numPlanets * 2;
		for (int p = 0; p < numPlanets; p++) {
			createPlanet(sun, planetCategoriesHuge, 300, 500, angleSlot * p * 2, angleSlot
					* (p * 2) + 1, 0.005f, 0.01f, depth);
		}

		planets.add(sun);
		for (Planet p : sun.getOrbitingPlanets()) {
			planets.add(p);

			for (Planet p1 : p.getOrbitingPlanets()) {
				planets.add(p1);

				for (Planet p2 : p1.getOrbitingPlanets()) {
					planets.add(p2);
				}
			}
		}
		
		sun.update(0);		

		return planets;
	}
	
	private static void updatePlanetNames(Planet sun){
		
		String[] latinDigits = new String[]{"I","II","III","IV","V","VI", "VII", "VIII","IX", "X"};
		
		for (Planet p : sun.getOrbitingPlanets()) {
			
			Faction faction = p.getFaction();
			
			if(faction != null){
				p.setName(faction.getHomePlanetName());
			}			

			for (Planet p1 : p.getOrbitingPlanets()) {
				p1.setName(p.getName() + " " + p1.getName());

				int counter = 0;
				for (Planet p2 : p1.getOrbitingPlanets()) {
					p2.setName(p1.getName() + " "  + latinDigits[counter++]);
				}
			}
		}
	}

	private void createPlanet(Planet attachedTo, PlanetCategory[] categories,
			float minDistanceToParent, float maxDistanceToParent,
			float minAngle, float maxAngle, float minVelocity,
			float maxVelocity, int depth) {
		
		PlanetCategory category = categories[MathUtils.random(categories.length-1)];
		float distanceToParent = MathUtils.random(minDistanceToParent,
				maxDistanceToParent);
		float angle = MathUtils.random(minAngle, maxAngle);
		float velocity = MathUtils.random(minVelocity, maxVelocity);		

		Planet planet = OrbiterFactory.getInstance().buildPlanet(category);
		planet.attachTo(attachedTo, distanceToParent, angle, velocity);				
		
		int size = planet.getCategory().getSize();

		depth--;		
		
		if (depth > 0) {
			int numPlanets = depth >1 ? MathUtils.random(2, 4) : MathUtils.random(1, 2);

			for (int p = 0; p < numPlanets; p++) {

				float d = (distanceToParent - size) *0.7f;
				float slot = d / (numPlanets * 2);

				createPlanet(planet, 
						depth >= 2 ? planetCategoriesMedium : planetCategoriesSmall, 
						size + slot * p * 2,
						size + slot * ((p * 2) + 1), 0, 
						6.28f, velocity * 2.1f,
						velocity * 4, depth);
			}
		}

	}
}
