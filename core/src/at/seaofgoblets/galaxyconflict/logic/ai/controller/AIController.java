package at.seaofgoblets.galaxyconflict.logic.ai.controller;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import at.seaofgoblets.galaxyconflict.logic.ai.event.AICommand;
import at.seaofgoblets.galaxyconflict.logic.ai.event.AISendFighterCommand;
import at.seaofgoblets.galaxyconflict.logic.model.LogicConfig;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class AIController {

	private final int ATTACK_WITH_CHILDREN = 0;
	private final int ATTACK_WITH_SISTERS = 1;
	private final int SEND_TO_PARENT = 2;
	private final int DO_NOTHING = 3;

	private final Array<Planet> planets;

	public AIController(Array<Planet> planets) {
		this.planets = planets;
	}

	public Array<AICommand> planNextAction(Planet planet) {

		Array<AICommand> aiCommands = new Array<AICommand>();

		int plan = MathUtils.random(4);

		float[] sendRates = new float[] { 0.5f, 0.75f, 0.875f };

		if (plan == ATTACK_WITH_CHILDREN) {
			// find children that help
			Array<Planet> attackingPlanets = new Array<Planet>();
			attackingPlanets.add(planet);
			for (Planet p : planet.getOrbitingPlanets()) {
				if (p.getFaction() == planet.getFaction()
						&& MathUtils.randomBoolean()) {
					attackingPlanets.add(p);
				}

				if (p.getFaction() == planet.getFaction()) {
					findChildren(p, attackingPlanets);
				}
			}

			// find target
			Planet targetPlanet = findTargetPlanet(planet);
			if (targetPlanet != null) {
				for (Planet p : attackingPlanets) {
					aiCommands.add(new AISendFighterCommand(p
							.getFighters(sendRates[MathUtils
									.random(sendRates.length - 1)]),
							targetPlanet));
				}
			}
		} else if (plan == ATTACK_WITH_SISTERS) {
			// find sisters that help
			Array<Planet> attackingPlanets = new Array<Planet>();
			attackingPlanets.add(planet);
			for (Planet p : planet.getParent().getOrbitingPlanets()) {
				if (p != planet && p.getFaction() == planet.getFaction()
						&& MathUtils.randomBoolean()) {
					attackingPlanets.add(p);
				}
			}

			// find target
			Planet targetPlanet = findTargetPlanet(planet);
			if (targetPlanet != null) {
				for (Planet p : attackingPlanets) {
					aiCommands.add(new AISendFighterCommand(p
							.getFighters(sendRates[MathUtils
									.random(sendRates.length - 1)]),
							targetPlanet));
				}
			}
		} else if (plan == SEND_TO_PARENT) {
			if (planet.getParent() != null
					&& planet.getParent().getCategory() != LogicConfig.PLANET_SUN) {
				aiCommands.add(new AISendFighterCommand(planet
						.getFighters(sendRates[MathUtils
								.random(sendRates.length - 1)]), planet
						.getParent()));
			}
		}

		return aiCommands;
	}

	private void findChildren(Planet planet, Array<Planet> children) {
		for (Planet p : planet.getOrbitingPlanets()) {
			if (p.getFaction() == planet.getFaction()
					&& MathUtils.randomBoolean()) {
				children.add(p);
			}

			if (p.getFaction() == planet.getFaction()) {
				findChildren(p, children);
			}
		}
	}

	private Planet findTargetPlanet(Planet srcPlanet) {
		Planet targetPlanet = null;

		final int PARENT = 0;
		final int SISTER = 1;
		final int CHILD = 2;
		final int NEAR_PLANET = 3;
		final int DO_NOTHING = 4;

		int option = MathUtils.random(4);

		switch (option) {
		case PARENT:
			if (srcPlanet.getParent() != null
					&& srcPlanet.getParent().getCategory() != LogicConfig.PLANET_SUN) {
				targetPlanet = srcPlanet.getParent();
			}
			break;
		case SISTER:
			Planet parent = srcPlanet.getParent();
			if (parent != null) {
				int idx = MathUtils
						.random(parent.getOrbitingPlanets().size - 1);
				if (parent.getOrbitingPlanets().get(idx) != srcPlanet) {
					targetPlanet = parent.getOrbitingPlanets().get(idx);
				}
			}
			break;
		case CHILD:
			if (srcPlanet.getOrbitingPlanets().size > 0) {
				int idx = MathUtils
						.random(srcPlanet.getOrbitingPlanets().size - 1);
				if (srcPlanet.getOrbitingPlanets().get(idx).getFaction() != srcPlanet
						.getFaction()) {
					targetPlanet = srcPlanet.getOrbitingPlanets().get(idx);
				}
			}
			break;
		case NEAR_PLANET:
			targetPlanet = findNearestHostilePlanet(srcPlanet);
			break;
		case DO_NOTHING:
		default:

		}

		return targetPlanet;
	}

	private Planet findNearestHostilePlanet(Planet srcPlanet) {
		Planet nearestPlanet = null;
		Vector2 srcPlanetPos = new Vector2(srcPlanet.getX(), srcPlanet.getY());
		Vector2 targetPos = new Vector2();
		float nearestPlanetDistance = 99999999;
		float distance;

		for (Planet p : planets) {
			if (p.getFaction() != srcPlanet.getFaction()
					&& p.getCategory() != LogicConfig.PLANET_SUN) {
				targetPos.set(p.getX(), p.getY());
				distance = targetPos.dst2(srcPlanetPos);

				if (distance < nearestPlanetDistance) {
					nearestPlanetDistance = distance;
					nearestPlanet = p;
				}
			}
		}

		return nearestPlanet;
	}
}
