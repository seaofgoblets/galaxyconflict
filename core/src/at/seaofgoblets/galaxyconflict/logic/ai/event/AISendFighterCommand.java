package at.seaofgoblets.galaxyconflict.logic.ai.event;

import com.badlogic.gdx.utils.Array;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class AISendFighterCommand implements AICommand{
	
	private final Planet targetPlanet;
	private final Array<Fighter> fighters;
	
	public AISendFighterCommand(Array<Fighter> fighters, Planet targetPlanet) {
		super();

		this.targetPlanet = targetPlanet;
		this.fighters = fighters;
	}

	public Planet getTargetPlanet() {
		return targetPlanet;
	}

	public Array<Fighter> getFighters() {
		return fighters;
	}		
}
