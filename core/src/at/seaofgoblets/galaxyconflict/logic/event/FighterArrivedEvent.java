package at.seaofgoblets.galaxyconflict.logic.event;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class FighterArrivedEvent implements LogicEvent{

	private final Fighter fighter;
	private final Planet planet;
	
	public FighterArrivedEvent(Fighter fighter, Planet planet) {
		super();
		this.fighter = fighter;
		this.planet = planet;
	}
	
	public Fighter getFighter() {
		return fighter;	
	}
	
	public Planet getPlanet() {
		return planet;
	}		
}
