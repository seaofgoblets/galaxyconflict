package at.seaofgoblets.galaxyconflict.logic.event;

import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class CreateFighterEvent implements LogicEvent{
	
	private final Planet planet;

	public CreateFighterEvent(Planet planet) {
		super();
		this.planet = planet;
	}

	public Planet getPlanet() {
		return planet;
	}	
}
