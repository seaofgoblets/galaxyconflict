package at.seaofgoblets.galaxyconflict.logic.event;

import com.badlogic.gdx.utils.Array;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class SendFightersEvent implements LogicEvent{

	private final Array<Fighter> fighters;
	private final Planet targetPlanet;
	
	public SendFightersEvent(Array<Fighter> fighters,  Planet targetPlanet) {
		super();
		this.fighters = fighters;
		this.targetPlanet = targetPlanet;
	}

	public Array<Fighter> getFighters() {
		return fighters;
	}

	public Planet getTargetPlanet() {
		return targetPlanet;
	}	
}
