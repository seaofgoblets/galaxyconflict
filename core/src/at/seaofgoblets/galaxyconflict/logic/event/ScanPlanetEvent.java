package at.seaofgoblets.galaxyconflict.logic.event;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class ScanPlanetEvent implements LogicEvent{

	private final Planet planet;
	private final Fighter fighter;
	
	public ScanPlanetEvent(Fighter fighter, Planet planet){
		this.fighter = fighter;
		this.planet = planet;		
	}
	
	public Planet getPlanet(){
		return this.planet;
	}

	public Fighter getFighter() {
		return fighter;
	}		
}
