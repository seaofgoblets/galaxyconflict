package at.seaofgoblets.galaxyconflict.logic.event;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;

public class FighterDestroyedEvent implements LogicEvent{
	private final Fighter fighter;
	
	public FighterDestroyedEvent(Fighter fighter) {
		super();
		this.fighter = fighter;
	}
	
	public Fighter getFighter() {
		return fighter;	
	}
}
