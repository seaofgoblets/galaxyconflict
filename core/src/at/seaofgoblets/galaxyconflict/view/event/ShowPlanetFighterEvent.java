package at.seaofgoblets.galaxyconflict.view.event;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class ShowPlanetFighterEvent implements ViewEvent{

	private final Planet planet;
	private final Fighter fighter;
	
	public ShowPlanetFighterEvent(Fighter fighter, Planet planet) {
		this.fighter = fighter;
		this.planet = planet;
	}
	
	public Planet getPlanet() {
		return planet;
	}

	public Fighter getFighter() {
		return fighter;
	}			
}
