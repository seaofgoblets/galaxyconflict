package at.seaofgoblets.galaxyconflict.view.event;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;

public class ShowIncomingFighterEvent implements ViewEvent {

	private final Fighter fighter;

	public ShowIncomingFighterEvent(Fighter fighter) {
		super();
		this.fighter = fighter;
	}

	public Fighter getFighter() {
		return fighter;
	}

}
