package at.seaofgoblets.galaxyconflict.view.event;

public interface ViewEventHandler {

	void addEvent(ViewEvent event);
}
