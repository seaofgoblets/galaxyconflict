package at.seaofgoblets.galaxyconflict.view.event;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class PlanetBattleEvent implements ViewEvent{

	private final Planet planet;
	private final Fighter fighter;

	public PlanetBattleEvent(Fighter fighter, Planet planet) {
		super();
		this.planet = planet;
		this.fighter = fighter;
	}

	public Planet getPlanet() {
		return planet;
	}

	public Fighter getFighter() {
		return fighter;
	}				
}
