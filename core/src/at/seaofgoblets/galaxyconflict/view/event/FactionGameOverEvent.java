package at.seaofgoblets.galaxyconflict.view.event;

import at.seaofgoblets.galaxyconflict.logic.model.Faction;

public class FactionGameOverEvent implements ViewEvent{

	private final Faction faction;

	public FactionGameOverEvent(Faction faction) {
		super();
		this.faction = faction;
	}

	public Faction getFaction() {
		return faction;
	}		
}
