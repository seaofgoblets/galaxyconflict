package at.seaofgoblets.galaxyconflict.view.event;

public class TouchUpEvent implements ViewEvent {

	public static final int BUTTON_RIGHT = 1;
	public static final int BUTTON_LEFT  = 0;
	
	private final int screenX;
	private final int screenY;
	private final int pointer;
	private final int button;
	
	public TouchUpEvent(int screenX, int screenY, int pointer, int button){
		this.screenX = screenX;
		this.screenY = screenY;
		this.pointer = pointer;
		this.button = button;
	}

	public int getScreenX() {
		return screenX;
	}

	public int getScreenY() {
		return screenY;
	}

	public int getPointer() {
		return pointer;
	}

	public int getButton() {
		return button;
	}		
}
