package at.seaofgoblets.galaxyconflict.view.event;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;

public class CreateFighterSpriteEvent implements ViewEvent{

	private final Fighter fighter;
	
	public CreateFighterSpriteEvent(Fighter fighter){
		this.fighter = fighter;
	}
	
	public Fighter getFighter(){
		return this.fighter;
	}
}
