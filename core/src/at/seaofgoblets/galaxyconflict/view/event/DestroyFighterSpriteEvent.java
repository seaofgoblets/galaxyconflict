package at.seaofgoblets.galaxyconflict.view.event;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;

public class DestroyFighterSpriteEvent implements ViewEvent{
	private final Fighter fighter;

	public DestroyFighterSpriteEvent(Fighter fighter) {
		this.fighter = fighter;
	}

	public Fighter getFighter() {
		return this.fighter;
	}
}
