package at.seaofgoblets.galaxyconflict.view.event;

import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class PlanetStatusChangedEvent implements ViewEvent{

	public static final int FACTION_CHANGED = 0;
	public static final int PRODUCTION_CHANGED = 1;
	
	private final Planet planet;
	private final int type;

	public PlanetStatusChangedEvent(Planet planet, int type) {
		super();
		this.planet = planet;
		this.type = type;
	}
	
	public Planet getPlanet() {
		return planet;
	}
	
	public int getType(){
		return this.type;
	}
}
