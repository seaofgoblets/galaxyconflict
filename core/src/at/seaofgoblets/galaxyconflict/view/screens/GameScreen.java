package at.seaofgoblets.galaxyconflict.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import at.seaofgoblets.galaxyconflict.GalaxyConflict;
import at.seaofgoblets.galaxyconflict.logic.controller.GalaxyGenerator;
import at.seaofgoblets.galaxyconflict.logic.controller.GameState;
import at.seaofgoblets.galaxyconflict.logic.controller.OrbiterFactory;
import at.seaofgoblets.galaxyconflict.logic.model.GameSettings;
import at.seaofgoblets.galaxyconflict.view.controller.GameInputProcessor;
import at.seaofgoblets.galaxyconflict.view.controller.ViewController;
import at.seaofgoblets.galaxyconflict.view.event.ViewEvent;
import at.seaofgoblets.galaxyconflict.view.model.GameCamera;
import at.seaofgoblets.galaxyconflict.view.model.ViewConfig;

public class GameScreen extends ScreenAdapter {
	
	private final GalaxyConflict game;

	private Viewport viewport;
	private GameCamera camera;

	private SpriteBatch batch;

	private Stage stage;

	private Label debugText;
	
	private GameState gameState;
	private ViewController viewController;
	
	public GameScreen(GalaxyConflict game) {
		this.game = game;
	}

	@Override
	public void resize(int width, int height) {	
		/*
		stage.getViewport().update(width, height, true);		 
		viewport.update((int)((ViewConfig.SCREEN_HEIGHT / (float)height) *  (float)width), height);
				
//		viewport.getCamera().viewportWidth = (ViewConfig.SCREEN_HEIGHT / (float)height) *  (float)width;
//		viewport.getCamera().viewportHeight = height;
//		viewport.getCamera().update();
		
		//camera.resize(width, height);
		//viewport.setCamera(camera.getCamera());
		viewport.apply();		*/
		
		//stage.getViewport().update(width, height, true);		
	}

	@Override
	public void show() {
		super.show();
		camera = new GameCamera();
		
		viewport = new FitViewport(ViewConfig.SCREEN_WIDTH, ViewConfig.SCREEN_HEIGHT, camera.getCamera());
		viewport.apply(true);

		batch = new SpriteBatch();	

		gameState = new GameState();
		OrbiterFactory.getInstance().setGameState(gameState);
		new GalaxyGenerator().generate(gameState, new GameSettings(5,4));				
		
		stage = new Stage(new ExtendViewport(ViewConfig.SCREEN_WIDTH, ViewConfig.SCREEN_HEIGHT), batch);
		
		GameInputProcessor gameInputProcessor = new GameInputProcessor(this);

		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(stage);
		multiplexer.addProcessor(gameInputProcessor);
		Gdx.input.setInputProcessor(multiplexer);
		
		viewController = new ViewController(this);
		viewController.init(game.getAssetManager(), stage, gameState, gameInputProcessor);

		Skin skin = game.getAssetManager().get("skin.json", Skin.class);

		debugText = new Label("", skin, "normal_text");
		debugText.setX(10);
		debugText.setY(ViewConfig.SCREEN_HEIGHT - 20);
		stage.addActor(debugText);
						
		camera.moveTo(gameState.getPlayerFaction().getPlanets().get(0));
	}

	@Override
	public void render(float delta) {
		delta *= gameState.getTimeLapseMultiplier();
		
		gameState.update(delta);
					
		viewController.update(delta);

		camera.update();
		
//		debugText.setText("FPS: " + Gdx.graphics.getFramesPerSecond() + " CAMERA  X: " + camera.getCamera().position.x + "  Y: "
//				+ camera.getCamera().position.y + "  ZOOM: " + camera.getCamera().zoom);
		
		//debugText.setText("TimeLapse: " + gameState.getTimeLapseMultiplier() + "x");
		
		Gdx.gl.glClearColor(0, 0, 0.1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.prepareBatch(batch);		

		viewController.render(batch);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		batch.dispose();
	}

	public void addEvent(ViewEvent event) {
		viewController.addEvent(event);
	}	

	public GameCamera getCamera() {
		return this.camera;
	}
}
