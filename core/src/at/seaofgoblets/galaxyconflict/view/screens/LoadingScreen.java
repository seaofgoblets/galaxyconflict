package at.seaofgoblets.galaxyconflict.view.screens;

import at.seaofgoblets.galaxyconflict.GalaxyConflict;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class LoadingScreen extends ScreenAdapter {

	private static final float WORLD_WIDTH = 640;
	private static final float WORLD_HEIGHT = 480;

	private static final float PROGRESS_BAR_WIDTH = 100;
	private static final float PROGRESS_BAR_HEIGHT = 25;

	private float progress = 0;

    private final GalaxyConflict game;
    
    private ShapeRenderer shapeRenderer;
    private Viewport viewport;
    private Camera camera;

    public LoadingScreen(GalaxyConflict game) {
        this.game = game;
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewport.update(width, height);
    }
    
    @Override
    public void show() {
        super.show();
        camera = new OrthographicCamera();
        camera.position.set(WORLD_WIDTH / 2, WORLD_HEIGHT / 2, 0);
        camera.update();
        viewport = new FitViewport(WORLD_WIDTH, WORLD_HEIGHT, camera);
        shapeRenderer = new ShapeRenderer();
        
        TextureParameter param = new TextureParameter();
        param.genMipMaps = true;
        
        AssetManager manager = game.getAssetManager();

        manager.load("background.png", Texture.class);
        manager.load("stars5.png", Texture.class);
        manager.load("GalaxyConflict.pack", TextureAtlas.class);
        manager.load("retro_12.fnt", BitmapFont.class); // TODO remove
        manager.load("arimo_12.fnt", BitmapFont.class);
        manager.load("arimo_14.fnt", BitmapFont.class);
        manager.load("arimo_24.fnt", BitmapFont.class);
        manager.load("skin.json", Skin.class, new SkinLoader.SkinParameter("GalaxyConflict.pack"));
        
        manager.load("sounds/juskiddink__distant-explosion.ogg", Sound.class);
        manager.load("sounds/kalisemorrison__scanner-beep.ogg", Sound.class);
        manager.load("sounds/harpoyume__spaceship-passing.ogg", Sound.class);          
        manager.load("sounds/infobandit__laser01.ogg", Sound.class);
        manager.load("sounds/nbs-dark__explosion.ogg", Sound.class);
        manager.load("sounds/danielnieto7__alert.ogg", Sound.class);
        
        manager.load("music/Void-Fleet.ogg", Music.class);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        update();
        clearScreen();
        draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        shapeRenderer.dispose();
    }

    private void update() {
        if (game.getAssetManager().update()) {
           game.setScreen(new GameScreen(game));
        } else {
            progress = game.getAssetManager().getProgress();
        }
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    private void draw() {
        shapeRenderer.setProjectionMatrix(camera.projection);
        shapeRenderer.setTransformMatrix(camera.view);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.rect(
                WORLD_WIDTH / 2 - PROGRESS_BAR_WIDTH / 2, WORLD_HEIGHT / 2 - PROGRESS_BAR_HEIGHT / 2,
                progress * PROGRESS_BAR_WIDTH, PROGRESS_BAR_HEIGHT);
        shapeRenderer.end();
    }
}
