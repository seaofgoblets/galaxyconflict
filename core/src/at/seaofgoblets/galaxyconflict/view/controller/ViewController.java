package at.seaofgoblets.galaxyconflict.view.controller;

import java.util.Comparator;

import at.seaofgoblets.galaxyconflict.logic.controller.GameState;
import at.seaofgoblets.galaxyconflict.logic.event.SendFightersEvent;
import at.seaofgoblets.galaxyconflict.logic.model.Faction;
import at.seaofgoblets.galaxyconflict.logic.model.Fighter;
import at.seaofgoblets.galaxyconflict.logic.model.LogicConfig;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;
import at.seaofgoblets.galaxyconflict.view.event.CreateFighterSpriteEvent;
import at.seaofgoblets.galaxyconflict.view.event.DestroyFighterSpriteEvent;
import at.seaofgoblets.galaxyconflict.view.event.FactionGameOverEvent;
import at.seaofgoblets.galaxyconflict.view.event.PlanetBattleEvent;
import at.seaofgoblets.galaxyconflict.view.event.PlanetStatusChangedEvent;
import at.seaofgoblets.galaxyconflict.view.event.ShowIncomingFighterEvent;
import at.seaofgoblets.galaxyconflict.view.event.ShowPlanetFighterEvent;
import at.seaofgoblets.galaxyconflict.view.event.TouchDownEvent;
import at.seaofgoblets.galaxyconflict.view.event.TouchUpEvent;
import at.seaofgoblets.galaxyconflict.view.event.ViewEvent;
import at.seaofgoblets.galaxyconflict.view.event.ViewEventHandler;
import at.seaofgoblets.galaxyconflict.view.model.FighterSprite;
import at.seaofgoblets.galaxyconflict.view.model.GameCamera;
import at.seaofgoblets.galaxyconflict.view.model.MessageLogWindow;
import at.seaofgoblets.galaxyconflict.view.model.PlanetSprite;
import at.seaofgoblets.galaxyconflict.view.model.PlanetWindow;
import at.seaofgoblets.galaxyconflict.view.model.PooledPlanetEffect;
import at.seaofgoblets.galaxyconflict.view.model.SunSprite;
import at.seaofgoblets.galaxyconflict.view.model.ViewConfig;
import at.seaofgoblets.galaxyconflict.view.screens.GameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Queue;

public class ViewController implements ViewEventHandler {

	private static final String TAG = ViewController.class.getName();
	
	private GameState gameState;

	private Array<PlanetSprite> planetSprites = new Array<PlanetSprite>();
	private ObjectMap<Planet, PlanetSprite> planetMap = new ObjectMap<Planet, PlanetSprite>();
	private Array<PlanetSprite> selectedPlanets = new Array<PlanetSprite>();

	private Array<FighterSprite> fighterSprites = new Array<FighterSprite>();
	private ObjectMap<Fighter, FighterSprite> fighterMap = new ObjectMap<Fighter, FighterSprite>();
	private TextureRegion fighterTexture;

	private Sprite background;

	private Sprite stars;

	private Queue<ViewEvent> viewEvents = new Queue<ViewEvent>();
	private ViewEvent currentViewEvent = null;

	private final GameCamera camera;

	private PlanetWindow planetWindow;
	//private PlanetWindow2 planetWindow2;
	//private PlanetContextMenu planetContextMenu;
	private MessageLogWindow messageLogWindow;
	private Label gameOver;
	private Label victory;

	private boolean drawSelectionRect = false;

	private ShapeRenderer shapeRenderer;

	private GameInputProcessor gameInputProcessor;

	private Array<PooledEffect> fighterExplosionParticleEffects;
	private ParticleEffectPool fighterExplosionParticlePool;
	
	private Array<PooledPlanetEffect> planetBattleParticleEffects;
	private ParticleEffectPool planetBattleParticlePool;
	
	private Array<ParticleEffect> particleEffects;
	private ParticleEffect targetPlanetParticleEffect;

	private Sound fighterExplosion;
	private Sound scanPlanet;
	private Sound sendFighter;
	private Sound alert;
	private Sound planetBattle1;
	private Sound planetBattle2;	
	
	private Music music;

	public ViewController(GameScreen screen) {
		this.camera = screen.getCamera();
	}

	public void init(AssetManager assetManager, Stage stage, final GameState state,
			final GameInputProcessor inputProcessor) {

		this.gameState = state;
		this.gameInputProcessor = inputProcessor;

		ParticleEffect fighterExplosionEffect = new ParticleEffect();
		fighterExplosionEffect.load(Gdx.files.internal("particles/fighter_explosion"), Gdx.files.internal(""));
		fighterExplosionEffect.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		fighterExplosionEffect.start();

		fighterExplosionParticlePool = new ParticleEffectPool(fighterExplosionEffect, 20, 70);
		fighterExplosionParticleEffects = new Array<PooledEffect>();					
		
		ParticleEffect planetBattleEffect = new ParticleEffect();
		planetBattleEffect.load(Gdx.files.internal("particles/planet_battle"), Gdx.files.internal(""));
		planetBattleEffect.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		planetBattleEffect.start();

		planetBattleParticlePool = new ParticleEffectPool(planetBattleEffect, 20, 70);
		planetBattleParticleEffects = new Array<PooledPlanetEffect>();
				
		targetPlanetParticleEffect = new ParticleEffect();
		targetPlanetParticleEffect.load(Gdx.files.internal("particles/target"),Gdx.files.internal(""));
		
		particleEffects = new Array<ParticleEffect>();
		particleEffects.add(targetPlanetParticleEffect);
		
		Texture bgTexture = assetManager.get("background.png", Texture.class);
		bgTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		bgTexture.setWrap(Texture.TextureWrap.MirroredRepeat, Texture.TextureWrap.MirroredRepeat);
		TextureRegion region = new TextureRegion(bgTexture);
		region.setRegion(0, 0, bgTexture.getWidth() * ViewConfig.NUM_TILES_X,
				bgTexture.getHeight() * ViewConfig.NUM_TILES_Y);

		background = new Sprite(region);

		Texture starsTexture = assetManager.get("stars5.png", Texture.class);
		starsTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		starsTexture.setWrap(Texture.TextureWrap.MirroredRepeat, Texture.TextureWrap.MirroredRepeat);
		TextureRegion starsRegion = new TextureRegion(starsTexture);
		starsRegion.setRegion(0, 0, starsTexture.getWidth() * ViewConfig.NUM_TILES_X*2,
				starsTexture.getHeight() * ViewConfig.NUM_TILES_Y*2);

		stars = new Sprite(starsRegion);

		TextureAtlas atlas = assetManager.get("GalaxyConflict.pack", TextureAtlas.class);

		// enable texture filtering for pixel smoothing
		for (Texture t : atlas.getTextures()) {
			t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		}

		TextureRegion[] sunTextures = new TextureRegion[3];
		sunTextures[0] = atlas.findRegion("sun1");
		sunTextures[1] = atlas.findRegion("sun2");
		sunTextures[2] = atlas.findRegion("sunglow");

		final int numPlanetTextures = 11;
		TextureRegion[] planetTextures = new TextureRegion[numPlanetTextures];
		for (int t = 0; t < numPlanetTextures; t++) {
			planetTextures[t] = atlas.findRegion("planet" + (t + 4));
		}

		fighterTexture = atlas.findRegion("fighter6");

		Array<Planet> planets = gameState.getPlanets();
		
		TextureRegion[] infrastructureLevelTextures = new TextureRegion[]{
				atlas.findRegion("infrastructure_level1"), 
				atlas.findRegion("infrastructure_level2"),
				atlas.findRegion("infrastructure_level3")
		}; 

		for (int p = 0; p < planets.size; p++) {

			Planet planet = planets.get(p);
			PlanetSprite sprite = null;
			if (planet.getCategory() == LogicConfig.PLANET_SUN) {
				sprite = new SunSprite(planet, sunTextures);
			} else {
				sprite = new PlanetSprite(planet, planetTextures[p % planetTextures.length],
						new Color(MathUtils.random(0.6f, 1), MathUtils.random(0.6f, 1), MathUtils.random(0.6f, 1), 1), infrastructureLevelTextures);
			}

			planetSprites.add(sprite);
			planetMap.put(planets.get(p), sprite);
		}
		
		Faction playerFaction = gameState.getPlayerFaction();
		for(Planet p : playerFaction.getPlanets()){
			for(Fighter f : p.getOrbitingFighters()){
				addEvent(new CreateFighterSpriteEvent(f));
			}
		}

		planetSprites.sort(new Comparator<PlanetSprite>() {
			@Override
			public int compare(PlanetSprite p1, PlanetSprite p2) {
				return p2.getPlanet().getCategory().getSize() - p1.getPlanet().getCategory().getSize();
			}
		});

		planetWindow = new PlanetWindow(stage, gameState, this, assetManager);
		planetWindow.setPosition(10, 10);
		
		messageLogWindow = new MessageLogWindow(stage, assetManager.get("skin.json", Skin.class));
		
		gameOver = new Label("GAME OVER!", assetManager.get("skin.json", Skin.class), "normal_text");		
		gameOver.setAlignment(Align.center);
		gameOver.setScale(3);
		gameOver.setPosition(ViewConfig.SCREEN_WIDTH/2-gameOver.getWidth()/2, ViewConfig.SCREEN_HEIGHT/2);
		gameOver.setVisible(false);
		stage.addActor(gameOver);
						
		Skin skin = assetManager.get("skin.json", Skin.class);
		
		victory = new Label("VICTORY!", skin, "normal_text");		
		victory.setAlignment(Align.center);
		victory.setScale(3);
		victory.setPosition(ViewConfig.SCREEN_WIDTH/2-victory.getWidth()/2, ViewConfig.SCREEN_HEIGHT/2);
		victory.setVisible(false);
		stage.addActor(victory);

		gameState.setViewEventHandler(this);

		shapeRenderer = new ShapeRenderer();

		fighterExplosion = assetManager.get("sounds/juskiddink__distant-explosion.ogg");
		scanPlanet = assetManager.get("sounds/kalisemorrison__scanner-beep.ogg");
		sendFighter = assetManager.get("sounds/harpoyume__spaceship-passing.ogg");
		alert = assetManager.get("sounds/danielnieto7__alert.ogg");
		planetBattle1 = assetManager.get("sounds/nbs-dark__explosion.ogg");
		planetBattle2 = assetManager.get("sounds/infobandit__laser01.ogg");
		
//		planetContextMenu = new PlanetContextMenu();
//		planetContextMenu.init(stage, skin, camera.getCamera());	
		
//		planetWindow = new PlanetWindow();
//		planetWindow.init(stage, skin);	
		
		music = assetManager.get("music/Void-Fleet.ogg");
		music.play();
		music.setLooping(true);
	}

	public void render(SpriteBatch batch) {
		
		float deltaTime = Gdx.graphics.getDeltaTime();
								
		
		Gdx.gl.glDisable(GL20.GL_BLEND);
		
		
		batch.begin();

		float backgroundX = camera.getCamera().position.x * 0.5f - ViewConfig.SCREEN_WIDTH;
		float backgroundY = camera.getCamera().position.y * 0.5f - ViewConfig.SCREEN_HEIGHT;

		batch.setColor(1,1,1, 1);
		batch.draw(background, backgroundX, backgroundY);
		
		batch.setColor(1, 1, 1, 1);

		backgroundX = camera.getCamera().position.x * 0.25f - ViewConfig.SCREEN_WIDTH;
		backgroundY = camera.getCamera().position.y * 0.25f - ViewConfig.SCREEN_WIDTH;

		batch.draw(stars, backgroundX, backgroundY);

		for (PlanetSprite planet : planetSprites) {
			planet.render(batch);
		}
		
//		for (PlanetSprite planetSprite : selectedPlanets) {		
//			planetSprite.renderProgressBar(batch);
//		}

		for (FighterSprite fighter : fighterSprites) {
			fighter.render(batch);
		}

		for (PooledEffect effect : fighterExplosionParticleEffects) {
			effect.draw(batch, deltaTime);
			if (effect.isComplete()) {
				fighterExplosionParticleEffects.removeValue(effect, true);
				effect.free();
			}
		}
		
		for(ParticleEffect effect : particleEffects){
			effect.update(deltaTime);
			effect.draw(batch);
		}		
		
		PooledEffect effect;
		Planet planet;
		for (PooledPlanetEffect planetEffect : planetBattleParticleEffects) {
			effect = planetEffect.getPooledEffect();
			planet =planetEffect.getPlanet();
			effect.setPosition(planet.getX(), planet.getY());
			effect.draw(batch, Gdx.graphics.getDeltaTime());
			if (effect.isComplete()) {
				planetBattleParticleEffects.removeValue(planetEffect, true);
				effect.free();
			}
		}				

		batch.end();

		camera.update();
		shapeRenderer.setProjectionMatrix(camera.getCamera().combined);

		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(Color.WHITE);
		for (PlanetSprite planetSprite : selectedPlanets) {
			planetSprite.renderSelected(shapeRenderer);		
		}

		if (drawSelectionRect) {
			Rectangle sectionRect = gameInputProcessor.getSelectionRect();
			shapeRenderer.rect(sectionRect.x, sectionRect.y, sectionRect.width, sectionRect.height);
		}

		shapeRenderer.end();
		
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(Color.WHITE);
		for (PlanetSprite planetSprite : selectedPlanets) {
			
			planetSprite.renderProgressBar(shapeRenderer);
		}

		shapeRenderer.end();
		
		
		//planetContextMenu.update();
	}

	public void update(float deltaTime) {

		handleInput();
		handleEvents();

		for (PlanetSprite planet : planetSprites) {
			planet.update(deltaTime);
		}
	}

	@Override
	public void addEvent(ViewEvent event) {
		viewEvents.addLast(event);
	}

	private PlanetSprite lastTargetPlanet = null;

	private void handleEvents() {
		
		while (viewEvents.size > 0) {
			if (currentViewEvent == null) {
				currentViewEvent = viewEvents.removeFirst();
			}

			if (currentViewEvent instanceof TouchDownEvent) {
				TouchDownEvent touchEvent = (TouchDownEvent) currentViewEvent;

				if (touchEvent.getButton() == TouchDownEvent.BUTTON_LEFT) {
					Vector3 touchPos = new Vector3(touchEvent.getScreenX(), touchEvent.getScreenY(), 0);
					camera.unproject(touchPos);

					PlanetSprite planet = getClickedPlanet(camera, touchPos);
					if (planet != null) {
						if (planet != lastTargetPlanet && !selectedPlanets.contains(planet, true)) {
							selectedPlanets.clear();
							selectedPlanets.add(planet);
							planetWindow.show(selectedPlanets.get(0).getPlanet());

							//planetWindow2.show(planet);
							//planetContextMenu.setPlanet(planet);
						}
					} else {
						drawSelectionRect = true;
					}
				}else{
					lastTargetPlanet = null;
				}
			} else if (currentViewEvent instanceof TouchUpEvent) {
				TouchUpEvent touchEvent = (TouchUpEvent) currentViewEvent;
				if (touchEvent.getButton() == TouchDownEvent.BUTTON_LEFT) {

					if (drawSelectionRect) {
						getClickedPlanets(gameInputProcessor.getSelectionRect());
					} else {
						if (selectedPlanets.size > 0) {
							Vector3 touchPos = new Vector3(touchEvent.getScreenX(), touchEvent.getScreenY(), 0);

							camera.unproject(touchPos);

							boolean sendFighters = false;
							PlanetSprite targetPlanet = getClickedPlanet(camera, touchPos);
							lastTargetPlanet = null;
							if (targetPlanet != null) {
								if (!selectedPlanets.contains(targetPlanet, true)) {

									lastTargetPlanet = targetPlanet;

									for (PlanetSprite p : selectedPlanets) {
						
										if (p.getPlanet().getFaction() == gameState.getPlayerFaction()) {

											Array<Fighter> fighters = p.getPlanet().getFighters(
													(int) ((p.getPlanet().getOrbitingFighters().size +1)* 0.5f));

											if(fighters.size > 0){
												gameState.addEvent(new SendFightersEvent(fighters,
													targetPlanet.getPlanet()));													
												sendFighters = true;
											}
										}
									}
								}
							}
							
							if(sendFighters){
								sendFighter.play();
								
								Vector2 position = targetPlanet.getCenter();								
								targetPlanetParticleEffect.getEmitters().first().setPosition(position.x, position.y);
								targetPlanetParticleEffect.start();
							}
						}
					}

					drawSelectionRect = false;
				}
			} else if (currentViewEvent instanceof CreateFighterSpriteEvent) {
				createFighterSprite(((CreateFighterSpriteEvent) currentViewEvent).getFighter());
			} else if (currentViewEvent instanceof DestroyFighterSpriteEvent) {
				Fighter destroyedFighter = ((DestroyFighterSpriteEvent) currentViewEvent).getFighter();
				removeFighterSprite(destroyedFighter);

				PooledEffect effect = fighterExplosionParticlePool.obtain();
				effect.setPosition(destroyedFighter.getX(), destroyedFighter.getY());
				fighterExplosionParticleEffects.add(effect);

				fighterExplosion.play();
				
				Planet planet = destroyedFighter.getHomePlanet();
				if(destroyedFighter.getFaction() == gameState.getPlayerFaction() && 
						planet.getNumAgonizingFighters() == 0 && planet.getFaction() != gameState.getPlayerFaction()){
					for(Fighter f : planet.getOrbitingFighters()){
						removeFighterSprite(f);
					}
					
					Gdx.app.log(TAG, "Hiding fighters at " + planet.getName() + " as the battle is over");
				}

			} else if (currentViewEvent instanceof PlanetStatusChangedEvent) {
				handlePlanetChangedEvent(((PlanetStatusChangedEvent) currentViewEvent));
			}else if (currentViewEvent instanceof ShowPlanetFighterEvent) {

				ShowPlanetFighterEvent showPlanetFighterEvent = (ShowPlanetFighterEvent) currentViewEvent;
				
				Planet planet = showPlanetFighterEvent.getPlanet();											
								
				boolean scanSoundPlayed = false;
				for (Fighter f : planet.getOrbitingFighters()) {
					FighterSprite sprite = fighterMap.get(f);

					if (sprite == null) {
						createFighterSprite(f);
						
						if(!scanSoundPlayed){
							scanPlanet.play();
							scanSoundPlayed = true;
						}
					}
				}
			} else if(currentViewEvent instanceof ShowIncomingFighterEvent){
				ShowIncomingFighterEvent showIncomingFighterEvent = (ShowIncomingFighterEvent) currentViewEvent;
				Fighter fighter = showIncomingFighterEvent.getFighter();
				FighterSprite sprite = fighterMap.get(fighter);

				if (sprite == null) {
					createFighterSprite(fighter);
				}
				
				alert.play();
				
			} else if(currentViewEvent instanceof PlanetBattleEvent){
				PlanetBattleEvent planetBattleEvent = (PlanetBattleEvent) currentViewEvent;
				
				Planet planet = planetBattleEvent.getPlanet();
				Fighter fighter = planetBattleEvent.getFighter();
				
				float planetSize = planet.getCategory().getSize();
				PooledEffect effect = planetBattleParticlePool.obtain();
				effect.setPosition(planet.getX(), planet.getY());
			
				ParticleEmitter emitter = effect.getEmitters().get(0);
				emitter.getSpawnHeight().setHigh(planetSize/2);
				emitter.getSpawnHeight().setLow(planetSize/2);
				emitter.getSpawnWidth().setHigh(planetSize/2);
				emitter.getSpawnWidth().setLow(planetSize/2);
				
				if(planet.getFaction() == gameState.getPlayerFaction() || fighter.getFaction() == gameState.getPlayerFaction()){
					planetBattle1.play();
					planetBattle2.play();
				}
				
				planetBattleParticleEffects.add(new PooledPlanetEffect(effect, planet));				
			}else if (currentViewEvent instanceof FactionGameOverEvent) {			
				FactionGameOverEvent factionGameOverEvent = (FactionGameOverEvent) currentViewEvent;
				Faction faction = factionGameOverEvent.getFaction();
				
				messageLogWindow.addMessage(faction.getName() + " was destroyed!", faction.getColor());	
				
				if(faction == gameState.getPlayerFaction()){
					gameOver.setVisible(true);
				}
				else if(gameState.getNumActiveFactions() == 1){
					victory.setVisible(true);
				}
			}

			currentViewEvent = null;
		}
	}

	private void handlePlanetChangedEvent(
			PlanetStatusChangedEvent planetChangedEvent) {
		Planet planet = planetChangedEvent.getPlanet();
		PlanetSprite sprite = planetMap.get(planet);
		
		if(planetChangedEvent.getType() == PlanetStatusChangedEvent.FACTION_CHANGED){
			Faction faction = planet.getFaction();
			sprite.updateFaction();
			messageLogWindow.addMessage(planet.getName() + " conquered by " + faction.getName(), faction.getColor());	
		}
		else if (planetChangedEvent.getType() == PlanetStatusChangedEvent.PRODUCTION_CHANGED){
			sprite.updateProduction();
		}
	}

	private void handleInput() {
		if (Gdx.input.isKeyPressed(Input.Keys.A)) {
			camera.increaseZoom();
		}
		if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
			camera.decreaseZoom();
		}
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			camera.translate(-3, 0);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
			camera.translate(3, 0);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
			camera.translate(0, -3);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
			camera.translate(0, 3);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.PLUS)) {
			gameState.incrementeTimeLapse();
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.MINUS)) {
			gameState.decrementeTimeLapse();
		}
	}

	private PlanetSprite getClickedPlanet(GameCamera camera, Vector3 touchPos) {
		PlanetSprite planet;

		for (int s = planetSprites.size - 1; s >= 0; s--) {
			planet = planetSprites.get(s);

			if (planet.contains(touchPos.x, touchPos.y) && planet.getPlanet().getCategory() != LogicConfig.PLANET_SUN) {
				return planetSprites.get(s);
			}
		}

		return null;
	}

	private void getClickedPlanets(Rectangle selectionRect) {
		PlanetSprite planet;

		selectedPlanets.clear();

		Faction playerFaction = gameState.getPlayerFaction();
		Faction planetFaction;
		for (int s = 0; s < planetSprites.size; s++) {
			planet = planetSprites.get(s);

			planetFaction = planet.getPlanet().getFaction();

			if (playerFaction == planetFaction && selectionRect.contains(planet.getCenter())) {
				selectedPlanets.add(planet);
			}
		}
	}

	private void createFighterSprite(Fighter fighter) {
		FighterSprite fighterSprite = new FighterSprite(fighter, fighterTexture);
		fighterSprites.add(fighterSprite);
		fighterMap.put(fighter, fighterSprite);
	}
	
	private void removeFighterSprite(Fighter destroyedFighter){
		FighterSprite sprite = fighterMap.get(destroyedFighter);

		fighterSprites.removeValue(sprite, true);
		fighterMap.remove(destroyedFighter);
	}
}
