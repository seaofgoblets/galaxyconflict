package at.seaofgoblets.galaxyconflict.view.controller;

import at.seaofgoblets.galaxyconflict.view.event.TouchDownEvent;
import at.seaofgoblets.galaxyconflict.view.event.TouchUpEvent;
import at.seaofgoblets.galaxyconflict.view.model.GameCamera;
import at.seaofgoblets.galaxyconflict.view.screens.GameScreen;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class GameInputProcessor implements InputProcessor {	
	
	private final GameScreen screen;
	private GameCamera camera;
	
	private Vector2 dragOld;
	
	private int pressedButton;

	public GameInputProcessor(GameScreen screen) {
		this.screen = screen;
		this.camera = screen.getCamera();
	}

	@Override
	public boolean scrolled(int amount) {
		if (amount == 1) {
			camera.increaseZoom();
		} else if (amount == -1) {
			camera.decreaseZoom();
		}

		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	
	private Vector3 leftMouseDown=new Vector3();
	private Vector3 currentLeftMouseDown=new Vector3();
	
	private Rectangle selectionRect = new Rectangle();
	
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		pressedButton = button;
		screen.addEvent(new TouchDownEvent(screenX, screenY, pointer, button));		
		if(button == TouchDownEvent.BUTTON_LEFT){
			leftMouseDown.set(screenX, screenY,0);
			
			
//			Vector2 size = Scaling.fit.apply(1024, 768, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
//	        float viewportX = (Gdx.graphics.getWidth() - size.x) / 2.0f;
//	        float viewportY = (Gdx.graphics.getHeight() - size.y) / 2.0f;
//	        float viewportWidth = size.x;
//	        float viewportHeight = size.y;
			
			camera.unproject(leftMouseDown);//,viewportX, viewportY,viewportWidth, viewportHeight);
			
			currentLeftMouseDown.set(leftMouseDown);
			updateSelectionRect();
		}
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		dragOld = null;
		screen.addEvent(new TouchUpEvent(screenX, screenY, pointer, button));
		return false;
	}
	
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		
		if(pressedButton == TouchDownEvent.BUTTON_RIGHT){
			Vector2 dragNew = new Vector2(screenX, screenY);
			if(dragOld != null){
				camera.translate(dragOld.x - dragNew.x, dragNew.y - dragOld.y);
			}
			
			dragOld = dragNew;
		}else{
			currentLeftMouseDown.set(screenX, screenY,0);
			camera.unproject(currentLeftMouseDown);
			
			updateSelectionRect();
		}					
		
        return false;
	}		

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		//Gdx.input.setCursorPosition(screenX, screenY);
		return false;
	}
	
	public Rectangle getSelectionRect(){
		return selectionRect;
	}
	
	private void updateSelectionRect(){
		float minX, maxX, minY, maxY;
		
		if(leftMouseDown.x < currentLeftMouseDown.x){
			minX=leftMouseDown.x;
			maxX=currentLeftMouseDown.x;
		}else{
			maxX=leftMouseDown.x;
			minX=currentLeftMouseDown.x;
		}
		
		if(leftMouseDown.y < currentLeftMouseDown.y){
			minY=leftMouseDown.y;
			maxY=currentLeftMouseDown.y;
		}else{
			maxY=leftMouseDown.y;
			minY=currentLeftMouseDown.y;
		}
		
		selectionRect.set(minX, minY, 
				maxX - minX, 
				maxY - minY);
	}
}