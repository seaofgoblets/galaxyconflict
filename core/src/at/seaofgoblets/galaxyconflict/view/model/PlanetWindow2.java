package at.seaofgoblets.galaxyconflict.view.model;

import at.seaofgoblets.galaxyconflict.logic.model.LogicConfig;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;
import at.seaofgoblets.galaxyconflict.logic.model.Planet.ProductionMode;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar.ProgressBarStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

@Deprecated
public class PlanetWindow2 {

	private Label nameLabel;
	private Label infrastructureLabel;
	private Label fighterLabel;
	
	private Image planetImage;
	
	private ButtonGroup<Button> buttonGroupProduction =new ButtonGroup<Button>();
	private Button infrastructureModeButton;	
	private Button fighterModeButton;
	
	private PlanetSprite planetSprite;

	public void init(Stage stage, Skin skin) {

		Group group = new Group();

		Image backgroundImage = new Image(skin, "planet_window");
		group.addActor(backgroundImage);

		nameLabel = new Label("Earth 2", skin, "big_text");
		nameLabel.setAlignment(Align.center);
		nameLabel.setWidth(100);
		nameLabel.setPosition(0, 100);
		group.addActor(nameLabel);
		
		infrastructureLabel = new Label("Level: II", skin, "medium_text");
		infrastructureLabel.setAlignment(Align.center);
		infrastructureLabel.setWidth(100);
		infrastructureLabel.setPosition(0, 80);
		group.addActor(infrastructureLabel);
		
		fighterLabel = new Label("Fighters: 12", skin, "medium_text");
		fighterLabel.setAlignment(Align.center);
		fighterLabel.setWidth(100);
		fighterLabel.setPosition(0, 60);
		group.addActor(fighterLabel);
		
		planetImage = new Image(skin,"planet10");			
		group.addActor(planetImage);
		
		infrastructureModeButton = new Button(skin, "infrastructure_button");
		infrastructureModeButton.setPosition(100, 40);
		infrastructureModeButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(planetSprite!= null){
					planetSprite.getPlanet().setProductionMode(ProductionMode.INFRASTRUCTURE);
				}
			}
		});
		group.addActor(infrastructureModeButton);
		
		fighterModeButton = new Button(skin, "fighter_button");
		fighterModeButton.setPosition(100, 80);
		fighterModeButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(planetSprite!= null){
					planetSprite.getPlanet().setProductionMode(ProductionMode.FIGHTER);
				}
			}
		});
		group.addActor(fighterModeButton);
		
		buttonGroupProduction.setMaxCheckCount(1);
		buttonGroupProduction.setMinCheckCount(1);
		buttonGroupProduction.add(infrastructureModeButton);	
		buttonGroupProduction.add(fighterModeButton);
		
		TextureRegionDrawable textureBar = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("progressbar_back.png"))));
		TextureRegionDrawable textureKnob = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("progressbar_knob.png"))));				
		ProgressBarStyle barStyle = new ProgressBarStyle(textureBar,textureKnob); 
		barStyle.knobBefore = barStyle.knob;
		//barStyle.
		barStyle.background = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("progressbar_empty.png"))));
		
	
		fighterProductionProgess = new ProgressBar(0, 10, 1, false, barStyle);
		fighterProductionProgess.setStepSize(1);
		//
		fighterProductionProgess.setValue(5);
	//	fighterProductionProgess.setWidth(10);
		fighterProductionProgess.setPosition(50, 100);
		group.addActor(fighterProductionProgess);

		stage.addActor(group);
	}

	public void show(PlanetSprite planetSprite) {
		this.planetSprite = planetSprite;
		
		Planet planet = planetSprite.getPlanet();
		this.nameLabel.setText(planet.getName());
		this.fighterLabel.setText("Fighters: "
				+ planet.getOrbitingFighters().size);
		this.infrastructureLabel.setText("Level: "
				+ planet.getInfrastructureLevel());
		
		Color color = planet.getFaction() != null ? planet.getFaction().getColor() : Color.WHITE;
		float size = planet.getCategory().getSize() * 2;
		float delta = (LogicConfig.PLANET_BIG.getSize() - size) /2;
		this.planetImage.setColor(color);		
		planetDrawable.setRegion(planetSprite.body);
		this.planetImage.setDrawable(planetDrawable);
		
		this.planetImage.setSize(size,size);
		this.planetImage.setX(140+delta);
		this.planetImage.setY(140+delta);
				
	}
	
	private TextureRegionDrawable planetDrawable = new TextureRegionDrawable();
	private ProgressBar fighterProductionProgess;
}
