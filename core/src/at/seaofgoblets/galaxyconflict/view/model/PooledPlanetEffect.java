package at.seaofgoblets.galaxyconflict.view.model;

import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;

import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class PooledPlanetEffect {
	
	private final PooledEffect pooledEffect;
	private final Planet planet;
	
	public PooledPlanetEffect(PooledEffect pooledEffect, Planet planet) {
		super();
		this.pooledEffect = pooledEffect;
		this.planet = planet;
	}

	public PooledEffect getPooledEffect() {
		return pooledEffect;
	}
	public Planet getPlanet() {
		return planet;
	}
}
