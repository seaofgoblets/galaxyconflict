package at.seaofgoblets.galaxyconflict.view.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class SunSprite extends PlanetSprite {
	
	private TextureRegion[] textures;
	
	private static final float glowSize =64;
	private static final float glowSizeHalf = glowSize/2;
	
	public SunSprite(Planet planet, TextureRegion[] textures) {
		super(planet, textures[0], new Color(1, 1, 1, 1),null);		
		this.textures = textures;
		rotationSpeed = 3;
	}

	public void render(SpriteBatch batch) {
						
		color.a = 0.9f;

		batch.setColor(color);
		batch.draw(textures[2], 
				planet.getX()-glowSizeHalf+2, planet.getY()-glowSizeHalf-2, 
				glowSizeHalf,glowSizeHalf, 
				glowSize,glowSize, 
				1, 1, 0);
		color.a = 1;
		batch.setColor(color);
		batch.draw(textures[0], 
				planet.getX()-body.getWidth() / 2, planet.getY()-body.getHeight() / 2, 
				body.getWidth() / 2, body.getHeight() / 2, 
				body.getWidth(),body.getHeight(), 
				1, 1, rotation);
		
		color.a = 0.5f;
		
		batch.setColor(color);
		batch.draw(textures[1], 
				planet.getX()-body.getWidth() / 2, planet.getY()-body.getHeight() / 2, 
				body.getWidth() / 2, body.getHeight() / 2, 
				body.getWidth(),body.getHeight(), 
				1, 1, rotation*2);
		
		
	}

	public void update(float deltaTime) {
		rotation = rotation + (deltaTime * rotationSpeed);
		
		super.update(deltaTime);
		//boundingCircle.setPosition(planet.getX(), planet.getY());
	}
}
