package at.seaofgoblets.galaxyconflict.view.model;

import at.seaofgoblets.galaxyconflict.logic.model.Planet;
import at.seaofgoblets.galaxyconflict.logic.model.Planet.ProductionMode;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar.ProgressBarStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

@Deprecated
public class PlanetContextMenu {
	private Group group;
	private PlanetSprite planetSprite;
	private OrthographicCamera camera;
	
	private Label nameLabel;
	private Label infrastructureLabel;
	private Label fighterLabel;
	
	private ButtonGroup<Button> buttonGroupProduction =new ButtonGroup<Button>();
	private Button infrastructureModeButton;	
	private Button fighterModeButton;
	
	private ProgressBar fighterProductionProgess; 
	
	public void init(Stage stage, Skin skin, OrthographicCamera	 camera){
		this.camera = camera;
		
		group = new Group();
			
		nameLabel = new Label("Earth 2", skin, "big_text");
		nameLabel.setAlignment(Align.center);
		nameLabel.setWidth(100);
		nameLabel.setPosition(0, 0);
		group.addActor(nameLabel);
		
		infrastructureLabel = new Label("Level: II", skin, "medium_text");
		infrastructureLabel.setAlignment(Align.center);
		infrastructureLabel.setWidth(100);
		infrastructureLabel.setPosition(0, -15);
		group.addActor(infrastructureLabel);
		
		fighterLabel = new Label("Fighters: 12", skin, "medium_text");
		fighterLabel.setAlignment(Align.center);
		fighterLabel.setWidth(100);
		fighterLabel.setPosition(0, -30);
		group.addActor(fighterLabel);
		
		infrastructureModeButton = new Button(skin, "infrastructure_button");
		infrastructureModeButton.setPosition(0, -80);
		infrastructureModeButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(planetSprite!= null){
					planetSprite.getPlanet().setProductionMode(ProductionMode.INFRASTRUCTURE);
				}
			}
		});
		group.addActor(infrastructureModeButton);
		
		fighterModeButton = new Button(skin, "fighter_button");
		fighterModeButton.setPosition(50, -80);
		fighterModeButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(planetSprite!= null){
					planetSprite.getPlanet().setProductionMode(ProductionMode.FIGHTER);
				}
			}
		});
		group.addActor(fighterModeButton);
		
		TextureRegionDrawable textureBar = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("progressbar2_back.png"))));
		TextureRegionDrawable textureKnob = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("progressbar_fighter.png"))));		
		ProgressBarStyle barStyle = new ProgressBarStyle(textureBar,textureKnob); 
		barStyle.knobBefore = barStyle.knob;
	
		fighterProductionProgess = new ProgressBar(0, 10, 1, false, barStyle);
		fighterProductionProgess.setValue(1);
	//	fighterProductionProgess.setWidth(10);
		fighterProductionProgess.setPosition(50, -100);
		group.addActor(fighterProductionProgess);
		
		buttonGroupProduction.setMaxCheckCount(1);
		buttonGroupProduction.setMinCheckCount(1);
		buttonGroupProduction.add(infrastructureModeButton);	
		buttonGroupProduction.add(fighterModeButton);
		
		stage.addActor(group);
	}
	
	public void setPlanet(PlanetSprite planetSprite){
		this.planetSprite = planetSprite;
		
		if(this.planetSprite != null){
			Planet planet = planetSprite.getPlanet();
			this.nameLabel.setText(planet.getName());
			this.fighterLabel.setText("Fighters: " + planet.getOrbitingFighters().size);
			this.infrastructureLabel.setText("Level: " + planet.getInfrastructureLevel());
		}				
	}
	
	public void update(){
		if(planetSprite != null){
			Vector2 planetPosition = planetSprite.getCenter();
			Vector3 pos2 = camera.project(new Vector3(planetPosition.x, planetPosition.y, 0));  
			group.setPosition(pos2.x+50, pos2.y);			
		}					
	}
}
