package at.seaofgoblets.galaxyconflict.view.model;

import at.seaofgoblets.galaxyconflict.logic.model.LogicConfig;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

public class PlanetSprite{
	
	public static final int FIGHTER = 0;
	public static final int INFRASTRUCTURE = 1;

	protected final Sprite body;
	protected final Planet planet;	
	protected Color color;
	
	protected Circle boundingCircle;	
	
	protected float rotation = 0;
	protected float rotationSpeed = 0.1f;
	
	protected Vector2 center = new Vector2();
	
	private PooledEffect battleEffect;
	private ParticleEmitter battleEffectEmitter;
	
	protected Sprite infrastructureLevel;
	protected TextureRegion[] infrastructureLevelTextures;
	
	private static final Color[] PROGRESSBAR_COLORS= new Color[]{new Color(0,1,1,1), new Color(0,1,0,1)};
	private static final float PROGRESSBAR_HEIGHT = 3;
	
	private float progressBarX; 
	private float progressBarY; 
	private Color progressColor = PROGRESSBAR_COLORS[LogicConfig.DEFAULT_PRODUCTION];
	
	public PlanetSprite(Planet planet, TextureRegion planetTexture, Color color, TextureRegion[] infrastructureLevelTextures) {
		this.planet = planet;
		this.body = new Sprite(planetTexture);
		int size = planet.getCategory().getSize();
		this.body.setSize(size, size);
		
		float radius = this.body.getWidth()/2;
		if(radius < 6)
			radius = 6;
		
		this.boundingCircle = new Circle(0,0,radius);	
		this.color = planet.getFaction() != null ? planet.getFaction().getColor() : color;
		
		progressBarX = - this.body.getWidth() / 2;
		progressBarY = - radius *2;
		
		if(infrastructureLevelTextures != null){
			this.infrastructureLevelTextures = infrastructureLevelTextures;
			this.infrastructureLevel = new Sprite(infrastructureLevelTextures[0]);
			infrastructureLevel.setSize(20, 15);			
		}
	}

	public void render(SpriteBatch batch) {
		batch.setColor(color);
		batch.draw(body, 
				planet.getX()-body.getWidth() / 2, planet.getY()-body.getHeight() / 2, 
				body.getWidth() / 2, body.getHeight() / 2, 
				body.getWidth(),body.getHeight(), 
				1, 1, rotation);						
	}
	

	public void update(float deltaTime){
		center.set(planet.getX(), planet.getY());
		boundingCircle.setPosition(center);
		if(battleEffectEmitter != null){
			battleEffectEmitter.setPosition(planet.getX(), planet.getY());
		}
		if(infrastructureLevel != null){
			infrastructureLevel.setPosition(planet.getX()-body.getWidth() / 2 +5, planet.getY()-body.getHeight() / 2 + 7.5f);
		}
	}
	
	public void renderProgressBar(ShapeRenderer shapeRenderer){
		shapeRenderer.rect(planet.getX() + progressBarX, planet.getY()+ progressBarY, 
				this.body.getWidth() * planet.getProductionPercentage(), PROGRESSBAR_HEIGHT, 
				progressColor,progressColor,progressColor,progressColor
				);
		
	}
	
	public void renderSelected(ShapeRenderer shapeRenderer){				 
		 shapeRenderer.rect(planet.getX() - this.body.getWidth()/2-5, planet.getY()-this.body.getHeight()/2-5, this.body.getWidth()+10, this.body.getHeight()+10);
	}
	
	public boolean contains(float x, float y ){	
		return boundingCircle.contains(x, y);
	}
	
	public Planet getPlanet(){
		return planet;
	}
	
	public void updateFaction(){
		this.color = planet.getFaction().getColor();
	}
	
	public void updateProduction(){
		this.progressColor = PROGRESSBAR_COLORS[planet.getProductionMode().ordinal()];
	}
	
	public Vector2 getCenter(){
		return center;
	}			
}
