package at.seaofgoblets.galaxyconflict.view.model;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector3;

import at.seaofgoblets.galaxyconflict.logic.model.Planet;

public class GameCamera {
	private OrthographicCamera camera;
	
	public GameCamera(){
		camera = new OrthographicCamera();
	}
	
	public void resize(int w, int h){
		//camera = new OrthographicCamera();
		camera.update();
	}
	
	public OrthographicCamera getCamera(){
		return camera;
	}
	
	public void update(){		
		camera.update();
	}
	
	public void prepareBatch(Batch batch){
		batch.setProjectionMatrix(camera.projection);
		batch.setTransformMatrix(camera.view);
	}
	
	public void moveTo(Planet planet){
		camera.position.set(new Vector3(planet.getX(), planet.getY(), 0));	
	}
	
	public void increaseZoom(){
		if(camera.zoom < ViewConfig.MAX_CAMERA_ZOOM)
			camera.zoom += ViewConfig.CAMERA_ZOOM_SPEED;
	}
	
	public void decreaseZoom(){
		if(camera.zoom > ViewConfig.MIN_CAMERA_ZOOM)
			camera.zoom -= ViewConfig.CAMERA_ZOOM_SPEED;
	}
	
	public void translate(float x, float y){
		
		if(camera.position.x + x < ViewConfig.MIN_CAMERA_POS_X || camera.position.x + x > ViewConfig.MAX_CAMERA_POS_X)
			x = 0;
		
		if(camera.position.y + y < ViewConfig.MIN_CAMERA_POS_Y || camera.position.y + y > ViewConfig.MAX_CAMERA_POS_Y)
			y = 0;
		
		camera.translate(x, y);
	}
	
	public void unproject(Vector3 position){
		camera.unproject(position);
		
	}
	
	public void unproject(Vector3 screenCoords, float viewportX, float viewportY, float viewportWidth, float viewportHeight){
		camera.unproject(screenCoords, viewportX, viewportY, viewportWidth, viewportHeight);
	}
}
