package at.seaofgoblets.galaxyconflict.view.model;

public class ViewConfig {

	public static final float SCREEN_WIDTH = 1024;
	public static final float SCREEN_HEIGHT = 768;
	
	// World
	public static final int NUM_TILES_X = 8;
	public static final int NUM_TILES_Y = 8;
	
	public static final int TILE_SIZE = 256;
	
	public static final int GALAXY_WIDTH = TILE_SIZE * NUM_TILES_X;
	public static final int GALAXY_HEIGHT = TILE_SIZE * NUM_TILES_Y;
	
	// Camera
	public static final float MAX_CAMERA_ZOOM = 2.0f;
	public static final float MIN_CAMERA_ZOOM = 0.31f;
	
	public static final float MIN_CAMERA_POS_X = 500;
	public static final float MIN_CAMERA_POS_Y = 540;
	
	public static final float MAX_CAMERA_POS_X = 1600;
	public static final float MAX_CAMERA_POS_Y = 1600;
	
	public static final float CAMERA_ZOOM_SPEED = 0.1f;

}
