package at.seaofgoblets.galaxyconflict.view.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;

public class MessageLogWindow {

	private final int maxMessages = 5;
	
	Array<Label> labels = new Array<Label>();
	
	public MessageLogWindow(Stage stage, Skin skin){
						
		Table table = new Table();
		
		for(int i = 0; i < maxMessages; i++){
			Label label = new Label("", skin, "normal_text");
			labels.add(label);
			table.add(label).height(14).width(100).align(Align.topLeft).row();
		}		
		
		table.setPosition(ViewConfig.SCREEN_WIDTH-340, ViewConfig.SCREEN_HEIGHT- 40);
		stage.addActor(table);
	}
	
	public void addMessage(String messageText, Color color){
		
		for(int i = maxMessages-1; i > 0; i--){
			Label l1 = labels.get(i-1);
			Label l2 = labels.get(i);
			
			l2.setText(l1.getText());
			l2.setColor(l1.getColor());
		}
		
		labels.get(0).setText(messageText);
		labels.get(0).setColor(color);
	}		
}
