package at.seaofgoblets.galaxyconflict.view.model;

import at.seaofgoblets.galaxyconflict.logic.controller.GameState;
import at.seaofgoblets.galaxyconflict.logic.model.Planet;
import at.seaofgoblets.galaxyconflict.logic.model.Planet.ProductionMode;
import at.seaofgoblets.galaxyconflict.logic.observer.Observer;
import at.seaofgoblets.galaxyconflict.logic.observer.ProductionCompletedMessage;
import at.seaofgoblets.galaxyconflict.logic.observer.ProductionModeChangedMessage;
import at.seaofgoblets.galaxyconflict.logic.observer.ProductionStepCompletedMessage;
import at.seaofgoblets.galaxyconflict.logic.observer.SubjectMessage;
import at.seaofgoblets.galaxyconflict.view.event.PlanetStatusChangedEvent;
import at.seaofgoblets.galaxyconflict.view.event.ViewEventHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar.ProgressBarStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

public class PlanetWindow extends Window implements Observer {

	private final GameState gameState;
	
	private Planet planet;
	
	private Label nameLabel;
	private Label factionLabel;
	
	private Label infrastructureLabel;
	private Label fighterLabel;
	
	private Label nameValue;
	private Label factionValue;
	
	private Label infrastructureValue;
	private Label fighterValue;
	
	// production
	private ButtonGroup<Button> buttonGroupProduction =new ButtonGroup<Button>();
	private Button infrastructureModeButton;	
	private Button fighterModeButton;
	
	private ProgressBar productionProgess;
	
	private Table table;
	
	private final ViewEventHandler viewEventHandler;

	public PlanetWindow(Stage _stage, GameState gameState, final ViewEventHandler viewEventHandler, AssetManager assetManager) {
		
		super("",assetManager.get("skin.json", Skin.class), "planet");
		
		this.gameState = gameState;	
		
		this.viewEventHandler = viewEventHandler;
						
		nameLabel = new Label("PLANET", this.getSkin(), "normal_text");		
		factionLabel = new Label("FACTION", this.getSkin(), "normal_text");
		infrastructureLabel = new Label("INFRASTRUCTURE", this.getSkin(), "normal_text");	
		fighterLabel = new Label("FIGHTER", this.getSkin(), "normal_text");
		
		nameValue = new Label("???", this.getSkin(), "normal_text");		
		factionValue = new Label("???", this.getSkin(), "normal_text");
		infrastructureValue = new Label("???", this.getSkin(), "normal_text");	
		fighterValue = new Label("???", this.getSkin(), "normal_text");
			
		infrastructureModeButton = new Button(this.getSkin(), "infrastructure_button");
		infrastructureModeButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(planet!= null){
					planet.setProductionMode(ProductionMode.INFRASTRUCTURE);
					viewEventHandler.addEvent(new PlanetStatusChangedEvent(planet, PlanetStatusChangedEvent.PRODUCTION_CHANGED));
				}
			}
		});
		
		fighterModeButton = new Button(this.getSkin(), "fighter_button");
		fighterModeButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(planet!= null){
					planet.setProductionMode(ProductionMode.FIGHTER);
					viewEventHandler.addEvent(new PlanetStatusChangedEvent(planet, PlanetStatusChangedEvent.PRODUCTION_CHANGED));
				}
			}
		});
		
		TextureRegionDrawable textureBar = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("progressbar_back.png"))));
		TextureRegionDrawable textureKnob = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("progressbar_knob.png"))));		
		ProgressBarStyle barStyle = new ProgressBarStyle(textureBar,textureKnob); 
		barStyle.knobBefore = barStyle.knob;
	
		productionProgess = new ProgressBar(0, 10, 1, false, barStyle);
		productionProgess.setValue(0);							
		
		table = new Table();
		table.setFillParent(true);
		table.defaults().height(15);		
		table.defaults().align(Align.left);
		
		table.add(nameLabel).width(150);
		table.add(nameValue).width(250).row();
		table.add(factionLabel).height(20);
		table.add(factionValue).height(20).row();
		table.add(infrastructureLabel);		
		table.add(infrastructureValue).row();
		table.add(fighterLabel);		
		table.add(fighterValue).row();
		table.add(new Label("PRODUCTION: ", this.getSkin(), "normal_text")).height(20);		
		table.add(productionProgess).width(100).row();
		
		Table productionTable = new Table();
		productionTable.padTop(30);
		productionTable.defaults().height(38);	
		productionTable.add(infrastructureModeButton);
		
		productionTable.add(fighterModeButton).row();;
		table.add(productionTable).colspan(2).row();
		
		this.add(table).width(300);
		
		this.addListener(new ClickListener() {
			
		});
		
		buttonGroupProduction.setMaxCheckCount(1);
		buttonGroupProduction.setMinCheckCount(1);
		buttonGroupProduction.add(infrastructureModeButton);	
		buttonGroupProduction.add(fighterModeButton);
		
		this.addListener(new ClickListener() {
			
		});
		
		_stage.addActor(this);
		
		this.setSize(400, 150);
		
		this.setVisible(false);
	}

	public void show(Planet planet) {
		
		if(this.planet != null){
			this.planet.removeObserver(this);
		}
		
		this.planet = planet;
		this.planet.addObserver(this);
				
		updateLabels();
		
		if(planet.getFaction() != null && gameState.getPlayerFaction() == planet.getFaction()){
			ProductionMode mode = planet.getProductionMode();
			switch(mode){
			case INFRASTRUCTURE:
				infrastructureModeButton.setChecked(true);
				break;
			case FIGHTER:
			default:
				fighterModeButton.setChecked(true);
				break;
			}
			
			for(Button button  : buttonGroupProduction.getButtons()){
				button.setVisible(true);
				button.setTouchable(Touchable.enabled);
			}
			productionProgess.setVisible(true);
		}else{
			for(Button button  : buttonGroupProduction.getButtons()){
				button.setVisible(false);
				button.setTouchable(Touchable.disabled);
			}
			productionProgess.setVisible(false);
		}					
		updateLabels();
		this.setVisible(true);
	}

	public void hide() {
		this.setVisible(false);
	}
	
	@Override
	public void update(SubjectMessage message) {
		
		if(message instanceof ProductionStepCompletedMessage){
			int step = ((ProductionStepCompletedMessage)message).getStep();
			productionProgess.setValue(step);
		}else{
			if(message instanceof ProductionCompletedMessage){
				int numSteps = ((ProductionCompletedMessage)message).getNumStepsNextProduction();
				productionProgess.setRange(0, numSteps);
				productionProgess.setValue(0);
			}
			else if(message instanceof ProductionModeChangedMessage){
				ProductionModeChangedMessage msg =(ProductionModeChangedMessage)message; 
				int currentStep = msg.getStep();
				int numSteps = msg.getTotalSteps();
				productionProgess.setRange(0, numSteps);
				productionProgess.setValue(currentStep);
			}									
		}	
		updateLabels();		
	}
	
	private void updateLabels(){
						
		this.nameValue.setText(planet.getName());
		if(planet.getFaction() != null){
			this.factionValue.setText(planet.getFaction().getName());
		}else{
			this.factionValue.setText("???");
		}
		
		String infrastructure = "???";
		String fighters = "???";
		if(planet.getFaction() != null && gameState.getPlayerFaction() == planet.getFaction()){
			infrastructure = "LEVEL " + planet.getInfrastructureLevel();
			fighters = planet.getOrbitingFighters().size + "";
		}
		
		this.infrastructureValue.setText(infrastructure);
		this.fighterValue.setText(fighters);
	}
}
