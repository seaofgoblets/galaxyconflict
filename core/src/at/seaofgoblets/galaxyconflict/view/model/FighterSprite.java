package at.seaofgoblets.galaxyconflict.view.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import at.seaofgoblets.galaxyconflict.logic.model.Fighter;

public class FighterSprite {

	private final Sprite body;
	private final Fighter fighter;	
	private Color color;
	
	private boolean visible;
	
	public FighterSprite(Fighter fighter, TextureRegion texture) {
		this.fighter = fighter;
		this.body = new Sprite(texture);						
		this.color = fighter.getFaction() != null ? fighter.getFaction().getColor() : new Color(1,1,1,1);
		this.body.setSize(texture.getRegionWidth()*0.35f,texture.getRegionHeight()*0.35f);
	}
	
	public void render(SpriteBatch batch) {
		batch.setColor(color);
		batch.draw(body, 
				fighter.getX()-body.getWidth() / 2, fighter.getY()-body.getHeight() / 2, 
				body.getWidth() / 2, body.getHeight() / 2, 
				body.getWidth(),body.getHeight(), 
				1, 1, fighter.getRotation()+270);						
	}
	
	public void setVisible(boolean visible){
		this.visible = visible;
	}
	
	public boolean isVisible(){
		return visible;
	}
}
